import * as React from "react";
import * as ReactDom from "react-dom";
import {isEqual} from "./util";

export interface MapProps {
    id?: string,
    className?: string,
    style?: Object,
    center?: google.maps.LatLng,
    zoom?: number,
    layers?: Object,
    onZoomChanged?: Function,
    onBoundsChanged?: Function,
    hiddenLayers?: Array<string>,
    bbox?: string
}

declare interface TWTMarkerOpts extends google.maps.MarkerOptions {
    info?: string
}

class TWTMarker extends google.maps.Marker {
    constructor(markerOpts: TWTMarkerOpts) {
        super(markerOpts);
    }
}

export default class Map extends React.Component<MapProps, any>{

    private map: google.maps.Map;
    private layers: Object;
    private arrowIcon: Object;
    private infoWindow: google.maps.InfoWindow;
    private points: Array<google.maps.Point>

    constructor() {
        super();
        this.map = null;
    }

    componentDidMount() {
        console.log('mounted');
        this.createMap();
        this.createArrowIcon();
        this.infoWindow = new google.maps.InfoWindow();
        if (this.props.layers != null) {
            for (let key in this.props.layers) {
                this.updateLayer(key);
            }
        }
    }

    componentWillUnmount() {
        console.log('will unmount');
        this.destroyMap();
    }

    componentDidUpdate(prevProps, prevState) {
        console.log("componentDidUpdate");
        let {layers, hiddenLayers, zoom, bbox} = this.props;
        let oldLayers = prevProps.layers;
        let oldHidden = prevProps.hiddenLayers;
        Object.keys(layers).forEach(key => {
            if (!isEqual(layers[key], oldLayers[key])) {
                this.updateLayer(key);
            }
        });
        if (!isEqual(oldHidden, hiddenLayers)) {
            Object.keys(layers).forEach(key => {
                if (oldHidden.indexOf(key) == -1 && hiddenLayers.indexOf(key) != -1) {
                    this.hideLayer(key);
                } else if (oldHidden.indexOf(key) != -1 && hiddenLayers.indexOf(key) == -1) {
                    this.showLayer(key);
                }
            });
        }
        if (!isEqual(prevProps.zoom, zoom)) {
            if (zoom != this.map.getZoom()) {
                this.map.setZoom(zoom);
            }
        }
        if (!isEqual(prevProps.bbox, bbox)) {
            let mapBbox = this.boundsToBbox(this.map.getBounds());
            let newBounds = this.bboxToBounds(bbox);
            if (bbox != mapBbox) {
                this.map.panToBounds(newBounds);
            }
        }
    }

    private updateLayer(name: string) {

        let {hiddenLayers} = this.props;

        this.hideLayer(name);
        this.layers[name] = [];
        let features: Array<any> = this.props.layers[name];
        let hiddenLayer = hiddenLayers == null ? false : hiddenLayers.indexOf(name) != -1;
        if (features != null) {
            features.forEach((feature) => {
                if (feature.wkt != null) {
                    let geos: Array<any> = this.addWkt(feature.wkt, feature.color, feature.arrow, hiddenLayer);
                    geos.forEach((geo) => {
                        this.layers[name].push(geo);
                    });
                } else {
                    let marker = this.addMarker(feature, hiddenLayer);
                    this.layers[name].push(marker);
                }
            });
        }
    }

    private hideLayer(name: string) {
        if (this.layers[name] !== undefined) {
            (this.layers[name] as Array<any>).forEach((feature) => {
                feature.setMap(null);
            })
        }
    }

    private showLayer(name: string) {
        let features: Array<any> = this.layers[name];
        if (features != null) {
            features.forEach((feature) => {
                feature.setMap(this.map);
            })
        }
    }

    private addMarker(feature, hiddenLayer) {
        const image = {
            url: feature.img,
            origin: new google.maps.Point(0, 0)
        };

        if (feature.w != null && feature.h != null) {
            image['scaledSize'] = new google.maps.Size(feature.w, feature.h);
        }

        if (feature.aw != null && feature.ah != null) {
            image['anchor'] = new google.maps.Point(feature.aw, feature.ah);
        }

        const marker = new TWTMarker({
            position: new google.maps.LatLng(feature.lat, feature.lon),
            icon: image,
            map: hiddenLayer ? null : this.map,
            title: feature.title,
            info: feature.info
        });

        if (feature.info != null) {
            google.maps.event.addListener(marker, 'click', () => {
                this.infoWindow.setContent(feature.info);
                this.infoWindow.setPosition(marker.getPosition());
                this.infoWindow.open(marker.getMap());
            });
        }

        return marker;
    }

    private addWkt(wkt: string, color: string, arrow: boolean, hiddenLayer: boolean): Array<any> {
        if (wkt != null && wkt.trim().length != 0) {

            const typeRegex = /[A-Z]+/;
            const elemRegex = /\(((\s+)?\-?[0-9]+(\.[0-9]*)?\s+\-?[0-9]+(\.[0-9]*)?(\s+)?\,?)+\)/g;
            const coordRegex = /(\-?[0-9]+(\.[0-9]*)?)\s+(\-?[0-9]+(\.[0-9]*)?)/g;

            const type = wkt.match(typeRegex)[0].toLowerCase();

            const elems = wkt.match(elemRegex);

            const geos = [];

            elems.forEach((elem) => {

                const ptsArray = [];
                let coords = null;
                while ((coords = coordRegex.exec(elem)) != null) {
                    ptsArray.push(new google.maps.LatLng(parseFloat(coords[3]), parseFloat(coords[1])));
                }

                const geoOpts = {
                    path: ptsArray,
                    strokeColor: color,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: color,
                    fillOpacity: 0.35,
                    //radius: 100,
                    center: ptsArray[0],
                    radius: Math.pow(2, (21 - this.map.getZoom())) * 140 * 0.0027,
                    type: type
                };

                if (arrow) {
                    geoOpts['icons'] = [this.arrowIcon];
                }

                let geo = null;
                if (type.indexOf('line') != -1) {
                    geo = new google.maps.Polyline(geoOpts);
                } else if (type.indexOf('point') != -1) {
                    geo = new google.maps.Circle(geoOpts);
                } else {
                    geo = new google.maps.Polygon(geoOpts);
                }

                if (!hiddenLayer) {
                    geo.setMap(this.map);
                }

                geos.push(geo);

            });

            return geos;
        }
    }

    private createMap() {
        console.log('create Map');
        this.layers = [];
        let {center, zoom, onZoomChanged, onBoundsChanged, bbox} = this.props;
        this.map = new google.maps.Map(ReactDom.findDOMNode(this), {
            center: this.props.center,
            zoom: this.props.zoom
        });
        google.maps.event.addListener(this.map, 'zoom_changed', () => {
            for (let key in this.layers) {
                (this.layers[key] as Array<any>).forEach((feature) => {
                    if (feature.type == 'point') {
                        var p = Math.pow(2, (21 - this.map.getZoom()));
                        feature.setRadius(p * 140 * 0.0027);
                    }
                });
            }
        });
        if (onZoomChanged !== undefined) {
            google.maps.event.addListener(this.map, 'zoom_changed', () => {
                onZoomChanged(this.map.getZoom());
            });
        }
        if (onBoundsChanged !== undefined) {
            google.maps.event.addListener(this.map, 'bounds_changed', () => {
                onBoundsChanged(this.boundsToBbox(this.map.getBounds()));
            });
        }
        if (bbox != null) {
            this.map.panToBounds(this.bboxToBounds(bbox));
        }
    }

    private bboxToBounds(bbox: string): google.maps.LatLngBounds {
        let values: Array<string> = bbox.split(",");
        let sw = new google.maps.LatLng(parseFloat(values[1]), parseFloat(values[0]));
        let ne = new google.maps.LatLng(parseFloat(values[3]), parseFloat(values[2]));
        return new google.maps.LatLngBounds(sw, ne);
    }

    private boundsToBbox(bounds: google.maps.LatLngBounds): string {
        let sw = bounds.getSouthWest();
        let ne = bounds.getNorthEast();
        return sw.lng() + "," + sw.lat() + "," + ne.lng() + "," + ne.lat();
    }

    private createArrowIcon() {
        this.arrowIcon = {
            icon: {
                path: 'M0 -3 L3 4 L0 2 L-3 4 Z',
                strokeColor: '#333',
                fillColor: '#fff',
                fillOpacity: 0.5
            },
            fillColor: 'black',
            repeat: '300px'
        };
    }

    private destroyMap() {
        console.log('destroy map');
        //this.map.setTarget(null);
        this.map = null;
    }

    render() {

        let {id, className, style} = this.props;

        return (
            <div id={id}
                className={className}
                style={style}></div>
        );
    }
}