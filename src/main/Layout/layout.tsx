import * as React from "react";
import {isEmpty} from "../util";
import Mdl from "../mdl";

import _Header from "./Header/header";
import _Navigation from "./Navigation/navigation";
import _Content from "./content";
import _Drawer from "./drawer";
import _Spacer from "./spacer";
import _Title from "./title";

export interface LayoutProps {
    id?: string,
    style?: Object,
    className?: string,
    children?: JSX.Element,
    fixedDrawer?: boolean,
    fixedHeader?: boolean,
    fixedTabs?: boolean,
    noDrawerButton?: boolean,
    noDesktopDrawerButton?: boolean
}

export default class Layout extends Mdl<LayoutProps, any>{

    public static Header = _Header;
    public static Navigation = _Navigation;
    public static Content = _Content;
    public static Drawer = _Drawer;
    public static Spacer = _Spacer;
    public static Title = _Title;

    render() {

        let {id, style, children, fixedDrawer, fixedHeader, fixedTabs, noDrawerButton, noDesktopDrawerButton} = this.props;

        let className = "mdl-layout mdl-js-layout";

        if (fixedDrawer) {
            className += " mdl-layout--fixed-drawer";
        }

        if (fixedHeader) {
            className += " mdl-layout--fixed-header";
        }

        if (fixedTabs) {
            className += " mdl-layout--fixed-tabs";
        }

        if (noDrawerButton) {
            className += " mdl-layout--no-drawer-button";
        }

        if (noDesktopDrawerButton) {
            className += " mdl-layout--no-desktop-drawer-button";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className}
                style={style}
                id={id}>
                {children}
            </div>
        );
    }
}
