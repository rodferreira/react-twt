import * as React from "react";
import {isEmpty} from "../util";
import Mdl from "../mdl";

export interface TitleProps {
    id?: string,
    style?: Object,
    className?: string,
    children?: JSX.Element,
    largeScreenOnly?: boolean,
    smallScreenOnly?: boolean
}

export default class Title extends Mdl<TitleProps, any>{
    render() {

        let {id, style, children, largeScreenOnly, smallScreenOnly} = this.props;

        let className = "mdl-layout-title";

        if (largeScreenOnly) {
            className += " mdl-layout--large-screen-only";
        }

        if (smallScreenOnly) {
            className += " mdl-layout--small-screen-only";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <span className={className}
                id={id}
                style={style}>
                {children}
            </span>
        );
    }
}