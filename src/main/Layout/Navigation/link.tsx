import * as React from "react";
import {isEmpty} from "../../util";
import Mdl from "../../mdl";

export interface LinkProps {
    id?: string,
    style?: Object,
    className?: string,
    children?: JSX.Element,
    href?: string
    largeScreenOnly?: boolean,
    smallScreenOnly?: boolean
}

export default class Link extends Mdl<LinkProps, any>{
    render() {

        let {id, style, children, largeScreenOnly, smallScreenOnly, href} = this.props;

        let className = "mdl-navigation__link";

        if (largeScreenOnly) {
            className += " mdl-layout--large-screen-only";
        }

        if (smallScreenOnly) {
            className += " mdl-layout--small-screen-only";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <a className={className}
                id={id}
                style={style}
                href={href}>
                {children}
            </a>
        );
    }
}