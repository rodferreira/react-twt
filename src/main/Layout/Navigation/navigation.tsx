import * as React from "react";
import {isEmpty} from "../../util";
import Mdl from "../../mdl";

import _Link from "./link";

export interface NavigationProps {
    id?: string,
    style?: Object,
    className?: string,
    children?: JSX.Element,
    largeScreenOnly?: boolean,
    smallScreenOnly?: boolean
}

export default class Navigation extends Mdl<NavigationProps, any>{

    public static Link = _Link;

    render() {

        let {id, style, children, largeScreenOnly, smallScreenOnly} = this.props;

        let className = "mdl-navigation";

        if (largeScreenOnly) {
            className += " mdl-layout--large-screen-only";
        }

        if (smallScreenOnly) {
            className += " mdl-layout--small-screen-only";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <nav className={className}
                id={id}
                style={style}>
                {children}
            </nav>
        );
    }
}