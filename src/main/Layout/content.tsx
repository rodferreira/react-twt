import * as React from "react";
import {isEmpty} from "../util";
import Mdl from "../mdl";

export interface ContentProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    largeScreenOnly?: boolean,
    smallScreenOnly?: boolean
}

export default class Content extends Mdl<ContentProps, any>{
    render() {

        let {id, style, children, largeScreenOnly, smallScreenOnly} = this.props;

        let className = "mdl-layout__content";

        if (largeScreenOnly) {
            className += " mdl-layout--large-screen-only";
        }

        if (smallScreenOnly) {
            className += " mdl-layout--small-screen-only";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <main className={className}
                id={id}
                style={style}>
                {children}
            </main>
        );
    }
}