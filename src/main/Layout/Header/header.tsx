import * as React from "react";
import {isEmpty} from "../../util";
import Mdl from "../../mdl";

import _Row from "./row";

export interface HeaderProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    transparent?: boolean,
    scroll?: boolean,
    waterfall?: boolean,
    waterfallHideTop?: boolean,
    layoutIcon?: string,
    largeScreenOnly?: boolean,
    smallScreenOnly?: boolean,
    headerSeamed?: boolean
}

export default class Header extends Mdl<HeaderProps, any>{

    public static Row = _Row;

    render() {

        let {id, style, children, transparent, scroll, waterfall, waterfallHideTop, layoutIcon, largeScreenOnly, smallScreenOnly, headerSeamed} = this.props;

        let className = "mdl-layout__header";

        if (transparent) {
            className += " mdl-layout__header--transparent";
        }

        if (scroll) {
            className += " mdl-layout__header--scroll";
        }

        if (waterfall) {
            className += " mdl-layout__header--waterfall";
        }

        if (waterfallHideTop) {
            className += " mdl-layout__header--waterfall-hide-top";
        }

        if (largeScreenOnly) {
            className += " mdl-layout--large-screen-only";
        }

        if (smallScreenOnly) {
            className += " mdl-layout--small-screen-only";
        }

        if (headerSeamed) {
            className += " mdl-layout__header--seamed";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <header className={className}
                id={id}
                style={style}>
                {!isEmpty(layoutIcon) ?
                    <img class="mdl-layout-icon" src={layoutIcon}/>
                    :
                    ''
                }
                {children}
            </header>
        );
    }
}