import * as React from "react";
import {isEmpty} from "../util";
import Mdl from "../mdl";

export interface DrawerProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    largeScreenOnly?: boolean,
    smallScreenOnly?: boolean
}

export default class Header extends Mdl<DrawerProps, any>{
    render() {

        let {id, style, children, largeScreenOnly, smallScreenOnly} = this.props;

        let className = "mdl-layout__drawer";

        if (largeScreenOnly) {
            className += " mdl-layout--large-screen-only";
        }

        if (smallScreenOnly) {
            className += " mdl-layout--small-screen-only";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className}
                id={id}
                style={style}>
                {children}
            </div>
        );
    }
}