import * as React from "react";
import Mdl from "../mdl";
import {isEmpty} from "../util";

import _Item from "./item";

export interface MenuProps {
    children?: JSX.Element,
    position?: string,
    className?: string,
    ripple?: boolean,
    style?: Object,
    id?: string,
    htmlFor: string
}

export default class Menu extends Mdl<MenuProps, any>{

    public static Item = _Item;

    render() {

        let {position, ripple, style, id, htmlFor, children} = this.props;

        let className = "mdl-menu mdl-js-menu";

        if (!isEmpty(position)) {
            className += " mdl-menu--" + position;
        }

        if (ripple) {
            className += " mdl-js-ripple-effect";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <ul className={className}
                style={style}
                id={id}
                htmlFor={htmlFor}>
                {children}
            </ul>
        );
    }
}