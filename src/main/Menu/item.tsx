import * as React from "react";
import Mdl from "../mdl";
import {isEmpty} from "../util";

export interface ItemProps {
    children?: JSX.Element,
    id?: string,
    divider?: boolean,
    className?: string,
    disabled?: boolean,
    style?: Object
}

export default class Item extends Mdl<ItemProps, any>{

    render() {

        let {divider, disabled, id, style, children} = this.props;

        let className = "mdl-menu__item";

        if (divider) {
            className += " mdl-menu__item--full-bleed-divider";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <li className={className}
                id={id}
                style={style}
                disabled={disabled}>
                {children}
            </li>
        );
    }
}