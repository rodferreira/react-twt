import * as React from "react";
import {isEmpty} from "../util";
import Mdl from "../mdl";

import _Section from "./Section/section";

export interface FooterProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    mini?: boolean
}

export default class Footer extends Mdl<FooterProps, any>{
    
    public static Section = _Section;
    
    render() {

        let {id, children, style, mini} = this.props;

        let footerType = mini ? "mini" : "mega";

        let className = "mdl-" + footerType + "-footer";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <footer className={className}
                id={id}
                style={style}>
                {children}
            </footer>
        );
    }
}