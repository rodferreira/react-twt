import * as React from "react";
import {isEmpty} from "../../util";
import Mdl from "../../mdl";

import _Heading from "./heading";
import _LinkList from "./link-list";

export interface SectionProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    mini?: boolean,
    position: string
}

export default class Section extends Mdl<SectionProps, any>{
    
    public static Heading = _Heading;
    public static LinkList = _LinkList;
    
    render() {

        let {id, children, style, mini, position} = this.props;

        let footerType = mini ? "mini" : "mega";

        let className = "mdl-" + footerType + "-footer__" + position + "-section";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className}
                id={id}
                style={style}>
                {children}
            </div>
        );
    }
}