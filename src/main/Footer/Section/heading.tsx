import * as React from "react";
import {isEmpty} from "../../util";
import Mdl from "../../mdl";

export interface HeadingProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    mini?: boolean
}

export default class Heading extends Mdl<HeadingProps, any>{
    render() {

        let {id, children, style, mini} = this.props;

        let footerType = mini ? "mini" : "mega";

        let className = "mdl-" + footerType + "-footer__heading";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <h1 className={className}
                id={id}
                style={style}>
                {children}
            </h1>
        );
    }
}