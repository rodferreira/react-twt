import * as React from "react";
import {isEmpty} from "../../util";
import Mdl from "../../mdl";

export interface LinkListProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    mini?: boolean
}

export default class LinkList extends Mdl<LinkListProps, any>{
    render() {

        let {id, children, style, mini} = this.props;

        let footerType = mini ? "mini" : "mega";

        let className = "mdl-" + footerType + "-footer__link-list";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <ul className={className}
                id={id}
                style={style}>
                {children}
            </ul>
        );
    }
}