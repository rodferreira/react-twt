import * as React from "react";
import Mdl from "./mdl";
import {isEmpty} from "./util";

export interface ButtonProps {
    accent?: boolean,
    children?: JSX.Element,
    className?: string,
    colored?: boolean,
    disabled?: boolean,
    fab?: boolean,
    htmlFor?: string,
    icon?: boolean,
    id?: string,
    miniFab?: boolean,
    onClick?: Function,
    primary?: boolean,
    raised?: boolean,
    ripple?: boolean,
    socialMega?: boolean,
    socialMini?: boolean,
    style?: Object
}

export default class Button extends Mdl<ButtonProps, any>{

    render() {

        let {style, id, htmlFor, onClick, children, ripple,
            fab, miniFab, raised, colored, primary, accent, icon, disabled,
            socialMega, socialMini} = this.props;

        var className = "mdl-button mdl-js-button";

        if (ripple) {
            className += " mdl-js-ripple-effect";
        }

        if (fab) {
            className += " mdl-button--fab";
        }

        if (miniFab) {
            className += " mdl-button--mini-fab";
        }

        if (raised) {
            className += " mdl-button--raised";
        }

        if (colored) {
            className += " mdl-button--colored";
        }

        if (primary) {
            className += " mdl-button--primary";
        }

        if (accent) {
            className += " mdl-button--accent";
        }

        if (icon) {
            className += " mdl-button--icon";
        }

        if (disabled) {
            className += " mdl-button--disabled";
        }
        
        if(socialMega){
            className += " mdl-mega-footer__social-btn";
        }
        
        if(socialMini){
            className += " mdl-mini-footer__social-btn";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <button className={className}
                style={style}
                id={id}
                htmlFor={htmlFor}
                onClick={e => {
                    if (onClick != null) {
                        onClick(e);
                    }
                } }>
                {children}
            </button>
        );
    }
}