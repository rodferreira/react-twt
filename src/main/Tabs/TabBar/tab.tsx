import * as React from "react";
import {isEmpty} from "../../util";
import Mdl from "../../mdl";

export interface TabProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    href?: string,
    isActive?: boolean,
    layout?: boolean
}

export default class Tab extends Mdl<TabProps, any>{
    render() {

        let {id, style, children, isActive, href, layout} = this.props;

        let className = layout ? "mdl-layout__tab" : "mdl-tabs__tab";

        if (isActive) {
            className += " is-active";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return <a href={href} className={className} id={id} style={style}>{children}</a>
    }
}