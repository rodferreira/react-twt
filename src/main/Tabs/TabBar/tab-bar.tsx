import * as React from "react";
import {isEmpty} from "../../util";
import Mdl from "../../mdl";

import _Tab from "./tab";

export interface TabBarProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    layout?: boolean,
    ripple?: boolean
}

export default class TabBar extends Mdl<TabBarProps, any>{
    
    public static Tab = _Tab;
    
    render() {

        let {id, style, children, layout, ripple} = this.props;

        let className = layout ? "mdl-layout__tab-bar" : "mdl-tabs__tab-bar";

        if (ripple) {
            className += " mdl-js-ripple-effect";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return <div className={className} id={id} style={style}>{children}</div>
    }
}