import * as React from "react";
import {isEmpty} from "../util";
import Mdl from "../mdl";

export interface PanelProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    isActive?: boolean,
    layout?: boolean
}

export default class Panel extends Mdl<PanelProps, any>{
    render() {

        let {id, style, children, isActive, layout} = this.props;

        let className = layout ? "mdl-layout__tab-panel" : "mdl-tabs__panel";

        if (isActive) {
            className += " is-active";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return <div className={className} id={id} style={style}>{children}</div>
    }
}