import * as React from "react";
import {isEmpty} from "../util";
import Mdl from "../mdl";

import _TabBar from "./TabBar/tab-bar";
import _Panel from "./panel";

export interface TabsProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    ripple?: boolean
}

export default class Tabs extends Mdl<TabsProps, any>{
    
    public static TabBar = _TabBar;
    public static Panel = _Panel;
    
    render() {

        let {id, style, children, ripple} = this.props;

        let className = "mdl-tabs mdl-js-tabs";

        if (ripple) {
            className += " mdl-js-ripple-effect";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return <div className={className} id={id} style={style}>{children}</div>
    }
}