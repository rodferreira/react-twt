import * as React from "react";
import {isEmpty} from "./util";
import Mdl from "./mdl";

export interface IconProps {
    style?: Object,
    className?: string,
    id?: string,
    item?:boolean,
    itemAvatar?:boolean
}

export default class Icon extends Mdl<any, any>{

    render() {

        let {style, id, children, item, itemAvatar} = this.props;

        var className = "material-icons ";
        
        if(item){
            className += " mdl-list__item-icon";
        }
        
        if(itemAvatar){
            className += " mdl-list__item-avatar";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <i className={className}
                style={style}
                id={id}>
                {children}
            </i>
        );
    }

}