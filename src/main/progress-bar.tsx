import * as React from "react";
import * as ReactDom from "react-dom";
import Mdl from "./mdl";
import {isEmpty} from "./util";

export interface ProgressBarProps {
    id?: string,
    className?: string,
    style?: Object,
    indeterminate?: boolean,
    progress?: number,
    buffer?: number
}

export default class ProgressBar extends Mdl<ProgressBarProps, any>{

    private progressBar: any;

    constructor() {
        super();
        this.progressBar = null;
    }

    componentDidMount() {
        super.componentDidMount();
        this.progressBar = ReactDom.findDOMNode(this);
        this.updateProgressStatus();
    }

    componentDidUpdate() {
        super.componentDidUpdate();
        this.updateProgressStatus();
    }

    updateProgressStatus() {
        if (this.props.progress != null) {
            this.progressBar.MaterialProgress.setProgress(this.props.progress);
        }
        if (this.props.buffer != null) {
            this.progressBar.MaterialProgress.setBuffer(this.props.buffer);
        }
    }

    render() {

        let {indeterminate, id, style} = this.props;

        let className = "mdl-progress mdl-js-progress";

        if (indeterminate) {
            className += " mdl-progress__indeterminate";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div id={id}
                className={className}
                style={style}></div>
        );
    }
}