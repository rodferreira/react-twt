import * as React from "react";
import Mdl from "../mdl";
import {isEmpty} from "../util";

export interface ThProps {
    id?: string,
    className?: string,
    style?: Object
    children?: JSX.Element,
    nonNumeric?: boolean
}

export default class Th extends Mdl<ThProps, any>{
    render() {

        let {id, style, children, nonNumeric} = this.props;

        let className = "";

        if (nonNumeric) {
            className += " mdl-data-table__cell--non-numeric";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <td className={className}
                id={id}
                style={style}>
                {children}
            </td>
        )
    }
}
