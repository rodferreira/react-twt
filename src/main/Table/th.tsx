import * as React from "react";
import Mdl from "../mdl";
import {isEmpty} from "../util";

export interface ThProps {
    id?: string,
    className?: string,
    style?: Object
    children?: JSX.Element,
    sortedAscending?: boolean,
    sortedDescending?: boolean,
    nonNumeric?: boolean
}

export default class Th extends Mdl<ThProps, any>{
    render() {

        let {id, style, children, sortedAscending, sortedDescending, nonNumeric} = this.props;

        let className = "";

        if (sortedAscending) {
            className += " mdl-data-table__header--sorted-ascending";
        }

        if (sortedDescending) {
            className += " mdl-data-table__header--sorted-descending";
        }

        if (nonNumeric) {
            className += " mdl-data-table__cell--non-numeric";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <th className={className}
                id={id}
                style={style}>
                {children}
            </th>
        )
    }
}
