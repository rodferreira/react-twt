import * as React from "react";
import Mdl from "../mdl";
import {isEmpty} from "../util";

import _Th from "./th";
import _Td from "./td";

export interface TableProps {
    id?: string,
    className?: string,
    style?: Object
    children?: JSX.Element,
    shadow?: number
}

export default class Table extends Mdl<TableProps, any>{

    public static Th = _Th;
    public static Td = _Td;

    render() {

        let {id, style, children, shadow} = this.props;

        let className = "mdl-data-table mdl-js-data-table";

        if (shadow != null) {
            className += " mdl-shadow--" + shadow + "dp";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <table className={className}
                id={id}
                style={style}>
                {children}
            </table>
        )
    }
}
