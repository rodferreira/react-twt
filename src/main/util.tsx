/**
 * ES6 Object.assign
 */
export const objectAssign = function (target: any, ...sources: any[]) {
    'use strict';
    if (target === undefined || target === null) {
        throw new TypeError('Cannot convert undefined or null to object');
    }

    var output = Object(target);
    for (var index = 1; index < arguments.length; index++) {
        var source = arguments[index];
        if (source !== undefined && source !== null) {
            for (var nextKey in source) {
                if (source.hasOwnProperty(nextKey)) {
                    output[nextKey] = source[nextKey];
                }
            }
        }
    }
    return output;
};

/**
 * isEmpty function
 */
export const isEmpty = function (value: any): boolean {
    if (value == null) {
        return true;
    }
    switch (typeof value) {
        case "string":
            return value.trim().length == 0;
        case "object":
            return value.length == 0;
    }
}

export const isEqual = (a: any, b: any) => {
    let t1 = typeof a;
    let t2 = typeof b;
    if (t1 != t2) {
        return false;
    }
    if (t1 == 'object') {
        if (a == null && b == null) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }
        let equal = true;
        if (a instanceof Array) {
            if(a.length != b.length){
                equal = false;
            }else{
                (a as Array<any>).forEach((value, i) => {
                    if (!isEqual(value, (b as Array<any>)[i])) {
                        equal = false;
                    }
                });
            }
        } else {
            Object.keys(a).forEach(key => {
                let v1 = a[key];
                let v2 = b[key];
                let tk1 = typeof v1;
                let tk2 = typeof v2;
                if (tk1 != tk2) {
                    equal = false;
                } else if (tk1 == 'object') {
                    if (!isEqual(v1, v2)) {
                        equal = false;
                    }
                } else {
                    if (v1 !== v2) {
                        equal = false;
                    }
                }
            });
        }
        return equal;
    } else {
        return a === b;
    }
}