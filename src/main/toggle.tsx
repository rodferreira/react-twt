import * as React from "react";
import Icon from "./icon";
import {isEmpty} from "./util";
import Mdl from "./mdl";

export interface ToggleProps {
    id: string,
    type?: string,
    ripple?: boolean,
    onChange?: Function,
    className?: string,
    checked?: boolean,
    children?: JSX.Element,
    name?: string
    value?: string
}

export default class Toggle extends Mdl<ToggleProps, any>{
    render() {

        let {type, ripple, id, checked, children, onChange, value, name} = this.props;

        let isRadio = type == 'radio';
        let isIcon = type == 'icon';

        let className = "mdl-" + type + (isIcon ? "-toggle" : "") + " mdl-js-" + type + (isIcon ? "-toggle" : "");

        let inputType = isRadio ? "radio" : "checkbox";

        let inputClass = "mdl-" + type + (isIcon ? "-toggle" : "") + "__" + (isRadio ? "button" : "input");

        let labelClass = "mdl-" + type + (isIcon ? "-toggle" : "") + "__label";

        if (ripple) {
            className += " mdl-js-ripple-effect";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <label className={className} htmlFor={id}>
                <input type={inputType}
                    id={id}
                    className={inputClass}
                    checked={checked}
                    onChange={onChange}
                    value={value}
                    name={name}/>
                {isIcon ?
                    <Icon className="mdl-icon-toggle__label">{children}</Icon>
                    :
                    <span className={labelClass}>{children}</span>
                }
            </label>
        );
    }
}