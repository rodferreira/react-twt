import * as React from "react";
import * as ReactDom from "react-dom";

declare const componentHandler: any; 

abstract class Mdl<P, S> extends React.Component<P, S>{
    componentDidMount() {
        //componentHandler.upgradeElement(ReactDom.findDOMNode(this));
        componentHandler.upgradeDom();
    }
    componentWillUnmount() {
        componentHandler.downgradeElements(ReactDom.findDOMNode(this));
    }
    componentDidUpdate() {
        //componentHandler.upgradeElement(ReactDom.findDOMNode(this));
        componentHandler.upgradeDom();
    }
}

export default Mdl;