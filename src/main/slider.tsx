import * as React from "react";
import Mdl from "./mdl";

export interface SliderProps {
    id?: string,
    className?: string,
    style?: Object,
    min?: number,
    max?: number,
    value?: number,
    step?: number,
    disabled?: boolean,
    onChange?: Function
}

export default class Slider extends Mdl<SliderProps, any>{
    render() {

        let {style, id, className, min, max, value, step, disabled, onChange} = this.props;

        return (
            <p style={style} className={className}>
                <input className="mdl-slider mdl-js-slider"
                    type="range"
                    id={id}
                    min={min}
                    max={max}
                    value={value}
                    step={step}
                    disabled={disabled}
                    onChange={onChange}/>
            </p>
        );
    }
}