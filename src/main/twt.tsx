import _Badge from "./badge";
import _Button from "./button";
import _Card from "./Card/card";
import _Dialog from "./Dialog/dialog";
import _Footer from "./Footer/footer";
import _Grid from "./Grid/grid";
import _Icon from "./icon";
import _Layout from "./Layout/layout";
import _List from "./List/list";
import _Logo from "./logo";
import _Map from "./map";
import _Menu from "./Menu/menu";
import _ProgressBar from "./progress-bar";
import _Slider from "./slider";
import _Snackbar from "./snackbar";
import _Spinner from "./spinner";
import _Table from "./Table/table";
import _Tabs from "./Tabs/tabs";
import _TextField from "./text-field";
import _Toggle from "./toggle";
import _Tootip from "./tooltip";
import * as _util from "./util";

/*
 * react-twt namespace
 */
namespace twt {
    export const Badge = _Badge;
    export const Button = _Button;
    export const Card = _Card;
    export const Dialog = _Dialog;
    export const Footer = _Footer;
    export const Grid = _Grid;
    export const Icon = _Icon;
    export const Slider = _Slider;
    export const Layout = _Layout;
    export const List = _List;
    export const Logo = _Logo;
    export const Map = _Map;
    export const Menu = _Menu;
    export const ProgressBar = _ProgressBar;
    export const Snackbar = _Snackbar;
    export const Spinner = _Spinner;
    export const Table = _Table;
    export const Tabs = _Tabs;
    export const TextField = _TextField;
    export const Toggle = _Toggle;
    export const Tooltip = _Tootip;
    export const util = _util;
}

export default twt;