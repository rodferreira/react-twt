import * as React from "react";
import Mdl from "../mdl";
import {isEmpty} from "../util";

import _Actions from "./actions";
import _Media from "./media";
import _Title from "./Title/title";
import _SupportingText from "./supporting-text";
import _Menu from "./menu";

export interface CardProps {
    children?: JSX.Element,
    shadow?: number,
    className?: string,
    style?: Object,
    id?: string
}

export default class Card extends Mdl<CardProps, any>{

    public static Actions = _Actions;
    public static Media = _Media;
    public static Title = _Title;
    public static SupportingText = _SupportingText;
    public static Menu = _Menu;

    render() {

        let {style, id, children, shadow} = this.props;

        let className = "mdl-card";

        if (shadow != null) {
            className += " mdl-shadow--" + shadow + "dp";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className}
                style={style}
                id={id}>
                {children}
            </div>
        );
    }
}