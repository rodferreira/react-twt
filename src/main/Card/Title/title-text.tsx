import * as React from "react";
import Mdl from "../../mdl";
import {isEmpty} from "../../util";

export interface TitleTextProps {
    children?: JSX.Element,
    id?: string,
    className?: string,
    style?: Object
}

export default class Title extends Mdl<TitleTextProps, any>{

    render() {

        let {id, style, children} = this.props;

        let className = "mdl-card__title-text";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <h2 className={className} id={id} style={style}>
                {children}
            </h2>
        );
    }
}