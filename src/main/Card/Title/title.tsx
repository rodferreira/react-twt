import * as React from "react";
import Mdl from "../../mdl";
import {isEmpty} from "../../util";

import _TitleText from "./title-text";
import _SubtitleText from "./subtitle-text";

export interface TitleProps {
    children?: JSX.Element,
    expand?: boolean,
    id?: string,
    border?: boolean,
    className?: string,
    style?: Object
}

export default class Title extends Mdl<TitleProps, any>{

    public static Text = _TitleText;
    public static Subtitle = _SubtitleText;

    render() {

        let {border, id, style, children, expand} = this.props;

        let className = "mdl-card__title";

        if (expand) {
            className += " mdl-card--expand";
        }

        if (border) {
            className += " mdl-card--border";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className} id={id} style={style}>
                {children}
            </div>
        );
    }
}