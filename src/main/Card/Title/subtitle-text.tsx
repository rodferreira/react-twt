import * as React from "react";
import Mdl from "../../mdl";
import {isEmpty} from "../../util";

export interface SubtitleTextProps {
    children?: JSX.Element,
    id?: string,
    className?: string,
    style?: Object
}

export default class SubtitleText extends Mdl<SubtitleTextProps, any>{

    render() {

        let {id, style, children} = this.props;

        let className = "mdl-card__subtitle-text";

       if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <h3 className={className} id={id} style={style}>
                {children}
            </h3>
        );
    }
}