import * as React from "react";
import Mdl from "../mdl";
import {isEmpty} from "../util";

export interface MenuProps {
    children?: JSX.Element,
    id?: string,
    border?: boolean,
    className?: string,
    style?: Object
}

export default class Menu extends Mdl<MenuProps, any>{

    render() {

        let {border, id, style, children} = this.props

        let className = "mdl-card__menu";

        if (border) {
            className += " mdl-card--border";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className} id={id} style={style}>
                {children}
            </div>
        );
    }
}