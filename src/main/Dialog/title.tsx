import * as React from "react";
import Mdl from "../mdl";
import {isEmpty} from "../util";

export interface TitleProps {
    children?: JSX.Element,
    id?: string,
    className?: string,
    style?: Object
}

export default class Title extends Mdl<TitleProps, any>{
    render() {

        let {id, style, children} = this.props;

        let className = "mdl-dialog__title";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <h4 className={className}
                id={id}
                style={style}>
                {children}
            </h4>
        );
    }
};