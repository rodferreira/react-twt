import * as React from "react";
import Mdl from "../mdl";
import {isEmpty} from "../util";

export interface ContentProps {
    children?: JSX.Element,
    id?: string,
    className?: string,
    style?: Object
}

export default class Content extends Mdl<ContentProps, any>{
    render() {

        let {id, style, children} = this.props;

        let className = "mdl-dialog__content";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className}
                id={id}
                style={style}>
                {children}
            </div>
        );
    }
}