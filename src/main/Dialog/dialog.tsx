import * as React from "react";
import * as ReactDom from "react-dom";
import {isEmpty} from "../util";
import Mdl from "../mdl";

import _Actions from "./actions";
import _Content from "./content";
import _Title from "./title";

declare const dialogPolyfill: any;

export interface DialogProps {
    children?: JSX.Element,
    modal?: boolean,
    open?: boolean,
    id?: string,
    style?: Object,
    className?: string
}

export default class Dialog extends Mdl<DialogProps, any>{

    public static Actions = _Actions;
    public static Content = _Content;
    public static Title = _Title;

    private dialog: any;

    constructor() {
        super();
        this.dialog = null;
    }

    componentDidMount() {
        super.componentDidMount();
        this.dialog = ReactDom.findDOMNode(this);
        if (this.dialog.show === undefined) {
            if (typeof dialogPolyfill != "undefined") {
                dialogPolyfill.registerDialog(this.dialog);
            }
        }
        if (this.props.open) {
            this.show();
        }
    }

    componentDidUpdate() {
        super.componentDidUpdate();
        if (this.props.open) {
            this.show();
        } else {
            this.dialog.close();
        }
    }

    show() {
        if (this.props.modal) {
            this.dialog.showModal();
        } else {
            this.dialog.show();
        }
    }

    render() {

        let {id, style, children} = this.props;

        let className = "mdl-dialog ";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <dialog className={className}
                id={id}
                style={style}>
                {children}
            </dialog>
        );
    }
}