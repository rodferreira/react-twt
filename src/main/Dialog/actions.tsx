import * as React from "react";
import Mdl from "../mdl";
import {isEmpty} from "../util";

export interface ActionsProps {
    children?: JSX.Element,
    fullWidth?: boolean,
    id?: string,
    className?: string,
    style?: Object
}

export default class Actions extends Mdl<ActionsProps, any> {
    render() {

        let {fullWidth, id, style, children} = this.props;

        let className = "mdl-dialog__actions";

        if (fullWidth) {
            className += " mdl-dialog__actions--full-width";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className}
                id={id}
                style={style}>
                {children}
            </div>
        );
    }
};