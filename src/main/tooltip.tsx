import * as React from "react";
import {isEmpty} from "./util";
import Mdl from "./mdl";

export interface TooltipProps {
    htmlFor: string,
    children?: JSX.Element,
    className?: string,
    large?: boolean,
    left?: boolean,
    right?: boolean,
    top?: boolean,
    bottom?: boolean
}

export default class Tooltip extends Mdl<TooltipProps, any>{

    render() {

        let {large, left, right, top, bottom, children, htmlFor} = this.props;

        let className = "mdl-tooltip";

        if (large) {
            className += " mdl-tooltip--large";
        }

        if (left) {
            className += " mdl-tooltip--left";
        }

        if (right) {
            className += " mdl-tooltip--right";
        }

        if (top) {
            className += " mdl-tooltip--top";
        }

        if (bottom) {
            className += " mdl-tooltip--bottom";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <span className={className} htmlFor={htmlFor}>
                {children}
            </span>
        );
    }

}