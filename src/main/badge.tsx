import * as React from "react";
import Mdl from "./mdl";
import {isEmpty} from "./util";

export interface BadgeProps {
    children?: JSX.Element,
    className?: string
    data?: string,
    href?: string,
    icon?: boolean,
    id?: string,
    noBackground?: boolean,
    onClick?: Function,
    overlap?: boolean,
    style?: Object,
    tagName?: string
}

export default class Badge extends Mdl<BadgeProps, any>{
    render() {

        let {href, id, data, style, onClick, children, icon, overlap, noBackground} = this.props;

        var className = "mdl-badge";

        if (icon) {
            className += " material-icons";
        }

        if (overlap) {
            className += " mdl-badge--overlap";
        }

        if (noBackground) {
            className += " mdl-badge--no-background";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        switch (this.props.tagName) {
            case "a":
                return (
                    <a href={href}
                        id={id}
                        className={className}
                        data-badge={data}
                        style={style}
                        onClick={(e: Event) => {
                            if (onClick != null) {
                                onClick(e);
                            }
                        } }>
                        {children}
                    </a>
                );
            case "div":
                return (
                    <div className={className}
                        id={id}
                        data-badge={data}
                        style={style}
                        onClick={(e: Event) => {
                            if (onClick != null) {
                                onClick(e);
                            }
                        } }>
                        {children}
                    </div>
                );
            default:
                return (
                    <span className={className}
                        id={id}
                        data-badge={data}
                        style={style}
                        onClick={(e: Event) => {
                            if (onClick != null) {
                                onClick(e);
                            }
                        } }>
                        {children}
                    </span>
                );
        }
    }
}