import * as React from "react";
import Mdl from "./mdl";
import {isEmpty} from "./util";

export interface SpinnerProps {
    id?: string,
    style?: Object,
    className?: string,
    isActive?: boolean,
    singleColor?: boolean
}

export default class Spinner extends Mdl<SpinnerProps, any>{

    render() {

        let {isActive, singleColor, id, style} = this.props;

        let className = "mdl-spinner mdl-js-spinner";

        if (isActive) {
            className += " is-active";
        }

        if (singleColor) {
            className += " mdl-spinner--single-color";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <p id={id}
                style={style}
                className={className}></p>
        );
    }

}