import * as React from "react";
import {isEmpty} from "../../../util";
import Mdl from "../../../mdl";

export interface TextBodyProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element
}

export default class TextBody extends Mdl<TextBodyProps, any>{
    render() {

        let {id, style, children} = this.props;

        let className = "mdl-list__item-text-body";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <span className={className}
                id={id}
                style={style}>
                {children}
            </span>
        );
    }
}