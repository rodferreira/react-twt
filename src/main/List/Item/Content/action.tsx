import * as React from "react";
import {isEmpty} from "../../../util";
import Mdl from "../../../mdl";

export interface ItemProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element
    secondary?: boolean
    anchor?: boolean
    href?: string,
    onClick?: Function
}

export default class Item extends Mdl<ItemProps, any>{
    render() {

        let {secondary, id, style, children, anchor, onClick, href} = this.props;

        let className = "mdl-list__item-" + (secondary ? "secondary" : "primary") + "-action";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        if (anchor) {
            return <a className={className}
                id={id}
                style={style}
                onClick={onClick}
                href={href}>
                {children}
            </a>;
        }
        return <span className={className}
            id={id}
            style={style}
            onClick={onClick}>
            {children}
        </span>
    }
}