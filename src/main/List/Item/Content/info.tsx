import * as React from "react";
import {isEmpty} from "../../../util";
import Mdl from "../../../mdl";

export interface InfoProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element
    secondary?: boolean
}

export default class Info extends Mdl<InfoProps, any>{
    render() {

        let {secondary, id, style, children} = this.props;

        let className = "mdl-list__item-" + (secondary ? "secondary" : "primary") + "-info";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <span className={className}
                  id={id}
                  style={style}>
                {children}
            </span>
        );
    }
}