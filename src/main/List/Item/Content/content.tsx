import * as React from "react";
import {isEmpty} from "../../../util";
import Mdl from "../../../mdl";

import _Action from "./action";
import _TextBody from "./text-body";
import _Info from "./info";

export interface ItemProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element
    secondary?: boolean
}

export default class Item extends Mdl<ItemProps, any>{
    
    public static Action = _Action;
    public static TextBody = _TextBody;
    public static Info = _Info;
    
    render() {

        let {secondary, id, style, children} = this.props;

        let className = "mdl-list__item-" + (secondary ? "secondary" : "primary") + "-content";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <span className={className}
                id={id}
                style={style}>
                {children}
            </span>
        );
    }
}