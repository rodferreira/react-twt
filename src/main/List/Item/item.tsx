import * as React from "react";
import {isEmpty} from "../../util";
import Mdl from "../../mdl";

import _Content from "./Content/content";
import _Subtitle from "./subtitle";

export interface ItemProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
    twoLine?: boolean,
    threeLine?: boolean,
    onClick?: Function
}

export default class Item extends Mdl<ItemProps, any>{

    public static Content = _Content;
    public static Subtitle = _Subtitle;

    render() {

        let {twoLine, threeLine, style, id, children, onClick} = this.props;

        let className = "mdl-list__item";

        if (twoLine) {
            className += " mdl-list__item--two-line";
        }

        if (threeLine) {
            className += " mdl-list__item--three-line";
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <li className={className}
                id={id}
                style={style}
                onClick={onClick}>
                {children}
            </li>
        );
    }

}