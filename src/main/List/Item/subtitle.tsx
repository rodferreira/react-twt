import * as React from "react";
import {isEmpty} from "../../util";
import Mdl from "../../mdl";

export interface SubtitleProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element,
}

export default class Subtitle extends Mdl<SubtitleProps, any>{

    render() {

        let {style, id, children} = this.props;

        let className = "mdl-list__item-sub-title";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <span className={className}
                id={id}
                style={style}>
                {children}
            </span>
        );
    }

}