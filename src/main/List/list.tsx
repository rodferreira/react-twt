import * as React from "react";
import Mdl from "../mdl";
import {isEmpty} from "../util";

import _Item from "./Item/item";

export interface ListProps {
    id?: string,
    className?: string,
    style?: Object,
    children?: JSX.Element
}

export default class List extends Mdl<ListProps, any>{

    public static Item = _Item;

    render() {

        let {children, id, style} = this.props;

        let className = "mdl-list";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <ul className={className}
                id={id}
                style={style}>
                {children}
            </ul>
        );
    }
}


