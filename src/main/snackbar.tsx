import * as React from "react";
import * as ReactDom from "react-dom";
import {isEmpty} from "./util";
import Mdl from "./mdl";

export interface SnackbarProps {
    id?: string,
    style?: Object,
    className?: string,
    message?: String,
    timeout?: number,
    actionHandler?: Function,
    actionText?: string,
    show?:boolean;
}

export default class Snackbar extends Mdl<SnackbarProps, any>{

    private snackbar: any;

    constructor() {
        super();
        this.snackbar = null;
    }

    componentDidMount() {
        super.componentDidMount();
        this.snackbar = ReactDom.findDOMNode(this);
        if(this.props.show){
            this.showSnackBar();
        }
    }

    componentDidUpdate() {
        super.componentDidUpdate();
        if(this.props.show){
            this.showSnackBar();
        }
    }

    private showSnackBar() {
        let {message, timeout, actionHandler, actionText} = this.props;
        let data = {
            message,
            timeout,
            actionHandler,
            actionText
        };
        this.snackbar.MaterialSnackbar.showSnackbar(data);
    }

    render() {

        let className = "mdl-snackbar mdl-js-snackbar";

        let {id, style} = this.props;

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className}
                id={id}
                style={style}>
                <div className="mdl-snackbar__text"></div>
                <button type="button" className="mdl-snackbar__action"></button>
            </div>
        );
    }
}

