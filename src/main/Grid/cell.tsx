import * as React from "react";
import {isEmpty} from "../util";
import Mdl from "../mdl";

export interface CellProps {
    id?: string,
    style?: Object,
    className?: string,
    children?: JSX.Element,
    devices?: Object,
    col: number
}

export default class Cell extends Mdl<CellProps, any>{
    render() {

        let {col, devices, id, style, children} = this.props;

        let className = "mdl-cell";

        className += " mdl-cell--" + col + "-col";

        if (devices != null) {
            Object.keys(devices).map((key: string) => {
                const size = devices[key];
                className += " mdl-cell--" + size + "-col-" + key;
            });
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className} id={id} style={style}>
                {children}
            </div>
        );
    }
}