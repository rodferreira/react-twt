import * as React from "react";
import {isEmpty} from "../util";
import Mdl from "../mdl";

import _Cell from "./cell";

export interface GridProps {
    id?: string,
    style?: Object,
    className?: string,
    children?: JSX.Element
}

export default class Grid extends Mdl<GridProps, any>{
    
    public static Cell = _Cell;
    
    render() {

        let {id, style, children} = this.props;

        let className = "mdl-grid";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div id={id}
                style={style}
                className={className}>
                {children}
            </div>
        );
    }
}