import * as React from "react";
import * as ReactDom from "react-dom";
import Mdl from "./mdl";
import Button from "./button";
import Icon from "./icon";
import {isEmpty} from "./util";

export interface TextFieldProps {
    id?: string,
    multiLine?: boolean,
    rows?: number,
    pattern?: string,
    error?: string,
    label?: string,
    floatingLabel?: boolean,
    maxRows?: number,
    icon?: string,
    isInvalid?: boolean,
    value?: string
    type?: string,
    onChange?: Function,
    disabled?: boolean,
    alignRight?: boolean,
    className?:string
}

export default class TextField extends Mdl<TextFieldProps, any>{

    componentDidMount() {
        if (this.props.multiLine) {
            this.fixTextArea();
        }
        super.componentDidMount();
    }

    componentDidUpdate() {
        if (this.props.multiLine) {
            this.fixTextArea();
        }
        super.componentDidUpdate();
    }

    private fixTextArea() {
        let compDom = ReactDom.findDOMNode(this);
        let textAreas = compDom.getElementsByTagName('textarea');
        if (this.props.maxRows != null) {
            textAreas[0].setAttribute('maxrows', this.props.maxRows.toString());
        }
    }

    private buildMainGroup() {

        let {icon, floatingLabel, isInvalid, type, id, pattern, value, onChange, rows, disabled, label, error, multiLine, alignRight} = this.props;

        let className = "mdl-textfield mdl-js-textfield";

        if (!isEmpty(icon)) {
            className = "mdl-textfield__expandable-holder";
        } else {
            if (floatingLabel) {
                className += " mdl-textfield--floating-label";
            }

            if (isInvalid) {
                className += " is-invalid";
            }

            if (alignRight) {
                className += " mdl-textfield--align-right";
            }
        }

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className}>
                {!multiLine ?
                    <input className="mdl-textfield__input"
                        type={type || "text"}
                        id={id}
                        pattern={pattern}
                        value={value}
                        onChange={onChange}
                        disabled={disabled}/>
                    :
                    <textarea className="mdl-textfield__input"
                        type={type}
                        rows={rows}
                        id={id}
                        pattern={pattern}
                        value={value}
                        onChange={onChange}
                        disabled={disabled}/>
                }
                <label className="mdl-textfield__label" htmlFor={id}>{label}</label>
                {error ?
                    <span className="mdl-textfield__error">{error}</span>
                    :
                    ''
                }
            </div>
        );

    }

    render() {

        let {icon, isInvalid, id, alignRight} = this.props;

        let mainGroup = this.buildMainGroup();

        let result: JSX.Element = null;

        if (isEmpty(icon)) {
            result = mainGroup;
        } else {
            result = <div className={"mdl-textfield mdl-js-textfield mdl-textfield--expandable" + (isInvalid ? " is-invalid" : "") + (alignRight ? " mdl-textfield--align-right" : "") }>
                <label className="mdl-button mdl-js-button mdl-button--icon" htmlFor={id}>
                    <Icon>{icon}</Icon>
                </label>
                {mainGroup}
            </div>
        }

        return result;
    }
}