import * as React from "react";
import {isEmpty} from "./util";
import Mdl from "./mdl";

export interface LogoProps{
    id?:string,
    className?: string,
    style?:Object,
    children?:JSX.Element
}

export default class Logo extends Mdl<LogoProps, any>{
    render() {

        let {id, children, style} = this.props;

        let className = "mdl-logo";

        if (!isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }

        return (
            <div className={className}
                id={id}
                style={style}>
                {children}
            </div>
        );
    }
}

