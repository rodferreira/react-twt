import Mdl from "./mdl";
export interface SpinnerProps {
    id?: string;
    style?: Object;
    className?: string;
    isActive?: boolean;
    singleColor?: boolean;
}
export default class Spinner extends Mdl<SpinnerProps, any> {
    render(): JSX.Element;
}
