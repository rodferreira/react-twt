"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("./mdl");
var util_1 = require("./util");
var Badge = (function (_super) {
    __extends(Badge, _super);
    function Badge() {
        _super.apply(this, arguments);
    }
    Badge.prototype.render = function () {
        var _a = this.props, href = _a.href, id = _a.id, data = _a.data, style = _a.style, onClick = _a.onClick, children = _a.children, icon = _a.icon, overlap = _a.overlap, noBackground = _a.noBackground;
        var className = "mdl-badge";
        if (icon) {
            className += " material-icons";
        }
        if (overlap) {
            className += " mdl-badge--overlap";
        }
        if (noBackground) {
            className += " mdl-badge--no-background";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        switch (this.props.tagName) {
            case "a":
                return (React.createElement("a", {href: href, id: id, className: className, "data-badge": data, style: style, onClick: function (e) {
                    if (onClick != null) {
                        onClick(e);
                    }
                }}, children));
            case "div":
                return (React.createElement("div", {className: className, id: id, "data-badge": data, style: style, onClick: function (e) {
                    if (onClick != null) {
                        onClick(e);
                    }
                }}, children));
            default:
                return (React.createElement("span", {className: className, id: id, "data-badge": data, style: style, onClick: function (e) {
                    if (onClick != null) {
                        onClick(e);
                    }
                }}, children));
        }
    };
    return Badge;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Badge;
//# sourceMappingURL=badge.js.map