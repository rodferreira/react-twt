import Mdl from "../mdl";
export interface TitleProps {
    children?: JSX.Element;
    id?: string;
    className?: string;
    style?: Object;
}
export default class Title extends Mdl<TitleProps, any> {
    render(): JSX.Element;
}
