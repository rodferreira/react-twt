"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("../mdl");
var util_1 = require("../util");
var Actions = (function (_super) {
    __extends(Actions, _super);
    function Actions() {
        _super.apply(this, arguments);
    }
    Actions.prototype.render = function () {
        var _a = this.props, fullWidth = _a.fullWidth, id = _a.id, style = _a.style, children = _a.children;
        var className = "mdl-dialog__actions";
        if (fullWidth) {
            className += " mdl-dialog__actions--full-width";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("div", {className: className, id: id, style: style}, children));
    };
    return Actions;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Actions;
;
//# sourceMappingURL=actions.js.map