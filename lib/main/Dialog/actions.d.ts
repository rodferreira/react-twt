import Mdl from "../mdl";
export interface ActionsProps {
    children?: JSX.Element;
    fullWidth?: boolean;
    id?: string;
    className?: string;
    style?: Object;
}
export default class Actions extends Mdl<ActionsProps, any> {
    render(): JSX.Element;
}
