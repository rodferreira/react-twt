import Mdl from "../mdl";
import _Actions from "./actions";
import _Content from "./content";
import _Title from "./title";
export interface DialogProps {
    children?: JSX.Element;
    modal?: boolean;
    open?: boolean;
    id?: string;
    style?: Object;
    className?: string;
}
export default class Dialog extends Mdl<DialogProps, any> {
    static Actions: typeof _Actions;
    static Content: typeof _Content;
    static Title: typeof _Title;
    private dialog;
    constructor();
    componentDidMount(): void;
    componentDidUpdate(): void;
    show(): void;
    render(): JSX.Element;
}
