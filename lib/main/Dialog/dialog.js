"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var ReactDom = require("react-dom");
var util_1 = require("../util");
var mdl_1 = require("../mdl");
var actions_1 = require("./actions");
var content_1 = require("./content");
var title_1 = require("./title");
var Dialog = (function (_super) {
    __extends(Dialog, _super);
    function Dialog() {
        _super.call(this);
        this.dialog = null;
    }
    Dialog.prototype.componentDidMount = function () {
        _super.prototype.componentDidMount.call(this);
        this.dialog = ReactDom.findDOMNode(this);
        if (this.dialog.show === undefined) {
            if (typeof dialogPolyfill != "undefined") {
                dialogPolyfill.registerDialog(this.dialog);
            }
        }
        if (this.props.open) {
            this.show();
        }
    };
    Dialog.prototype.componentDidUpdate = function () {
        _super.prototype.componentDidUpdate.call(this);
        if (this.props.open) {
            this.show();
        }
        else {
            this.dialog.close();
        }
    };
    Dialog.prototype.show = function () {
        if (this.props.modal) {
            this.dialog.showModal();
        }
        else {
            this.dialog.show();
        }
    };
    Dialog.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children;
        var className = "mdl-dialog ";
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("dialog", {className: className, id: id, style: style}, children));
    };
    Dialog.Actions = actions_1.default;
    Dialog.Content = content_1.default;
    Dialog.Title = title_1.default;
    return Dialog;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Dialog;
//# sourceMappingURL=dialog.js.map