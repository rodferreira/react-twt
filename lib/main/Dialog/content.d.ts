import Mdl from "../mdl";
export interface ContentProps {
    children?: JSX.Element;
    id?: string;
    className?: string;
    style?: Object;
}
export default class Content extends Mdl<ContentProps, any> {
    render(): JSX.Element;
}
