"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("./util");
var mdl_1 = require("./mdl");
var Tooltip = (function (_super) {
    __extends(Tooltip, _super);
    function Tooltip() {
        _super.apply(this, arguments);
    }
    Tooltip.prototype.render = function () {
        var _a = this.props, large = _a.large, left = _a.left, right = _a.right, top = _a.top, bottom = _a.bottom, children = _a.children, htmlFor = _a.htmlFor;
        var className = "mdl-tooltip";
        if (large) {
            className += " mdl-tooltip--large";
        }
        if (left) {
            className += " mdl-tooltip--left";
        }
        if (right) {
            className += " mdl-tooltip--right";
        }
        if (top) {
            className += " mdl-tooltip--top";
        }
        if (bottom) {
            className += " mdl-tooltip--bottom";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("span", {className: className, htmlFor: htmlFor}, children));
    };
    return Tooltip;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Tooltip;
//# sourceMappingURL=tooltip.js.map