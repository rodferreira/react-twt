"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("./util");
var mdl_1 = require("./mdl");
var Logo = (function (_super) {
    __extends(Logo, _super);
    function Logo() {
        _super.apply(this, arguments);
    }
    Logo.prototype.render = function () {
        var _a = this.props, id = _a.id, children = _a.children, style = _a.style;
        var className = "mdl-logo";
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("div", {className: className, id: id, style: style}, children));
    };
    return Logo;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Logo;
//# sourceMappingURL=logo.js.map