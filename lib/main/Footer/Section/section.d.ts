import Mdl from "../../mdl";
import _Heading from "./heading";
import _LinkList from "./link-list";
export interface SectionProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    mini?: boolean;
    position: string;
}
export default class Section extends Mdl<SectionProps, any> {
    static Heading: typeof _Heading;
    static LinkList: typeof _LinkList;
    render(): JSX.Element;
}
