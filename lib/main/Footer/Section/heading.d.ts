import Mdl from "../../mdl";
export interface HeadingProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    mini?: boolean;
}
export default class Heading extends Mdl<HeadingProps, any> {
    render(): JSX.Element;
}
