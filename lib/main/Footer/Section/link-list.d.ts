import Mdl from "../../mdl";
export interface LinkListProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    mini?: boolean;
}
export default class LinkList extends Mdl<LinkListProps, any> {
    render(): JSX.Element;
}
