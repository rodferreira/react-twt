"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../../util");
var mdl_1 = require("../../mdl");
var LinkList = (function (_super) {
    __extends(LinkList, _super);
    function LinkList() {
        _super.apply(this, arguments);
    }
    LinkList.prototype.render = function () {
        var _a = this.props, id = _a.id, children = _a.children, style = _a.style, mini = _a.mini;
        var footerType = mini ? "mini" : "mega";
        var className = "mdl-" + footerType + "-footer__link-list";
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("ul", {className: className, id: id, style: style}, children));
    };
    return LinkList;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = LinkList;
//# sourceMappingURL=link-list.js.map