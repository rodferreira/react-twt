"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../util");
var mdl_1 = require("../mdl");
var section_1 = require("./Section/section");
var Footer = (function (_super) {
    __extends(Footer, _super);
    function Footer() {
        _super.apply(this, arguments);
    }
    Footer.prototype.render = function () {
        var _a = this.props, id = _a.id, children = _a.children, style = _a.style, mini = _a.mini;
        var footerType = mini ? "mini" : "mega";
        var className = "mdl-" + footerType + "-footer";
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("footer", {className: className, id: id, style: style}, children));
    };
    Footer.Section = section_1.default;
    return Footer;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Footer;
//# sourceMappingURL=footer.js.map