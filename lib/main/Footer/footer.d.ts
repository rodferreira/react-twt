import Mdl from "../mdl";
import _Section from "./Section/section";
export interface FooterProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    mini?: boolean;
}
export default class Footer extends Mdl<FooterProps, any> {
    static Section: typeof _Section;
    render(): JSX.Element;
}
