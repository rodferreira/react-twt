"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var ReactDom = require("react-dom");
var util_1 = require("./util");
var mdl_1 = require("./mdl");
var Snackbar = (function (_super) {
    __extends(Snackbar, _super);
    function Snackbar() {
        _super.call(this);
        this.snackbar = null;
    }
    Snackbar.prototype.componentDidMount = function () {
        _super.prototype.componentDidMount.call(this);
        this.snackbar = ReactDom.findDOMNode(this);
        if (this.props.show) {
            this.showSnackBar();
        }
    };
    Snackbar.prototype.componentDidUpdate = function () {
        _super.prototype.componentDidUpdate.call(this);
        if (this.props.show) {
            this.showSnackBar();
        }
    };
    Snackbar.prototype.showSnackBar = function () {
        var _a = this.props, message = _a.message, timeout = _a.timeout, actionHandler = _a.actionHandler, actionText = _a.actionText;
        var data = {
            message: message,
            timeout: timeout,
            actionHandler: actionHandler,
            actionText: actionText
        };
        this.snackbar.MaterialSnackbar.showSnackbar(data);
    };
    Snackbar.prototype.render = function () {
        var className = "mdl-snackbar mdl-js-snackbar";
        var _a = this.props, id = _a.id, style = _a.style;
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("div", {className: className, id: id, style: style}, 
            React.createElement("div", {className: "mdl-snackbar__text"}), 
            React.createElement("button", {type: "button", className: "mdl-snackbar__action"})));
    };
    return Snackbar;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Snackbar;
//# sourceMappingURL=snackbar.js.map