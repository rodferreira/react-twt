import Mdl from "../mdl";
export interface ItemProps {
    children?: JSX.Element;
    id?: string;
    divider?: boolean;
    className?: string;
    disabled?: boolean;
    style?: Object;
}
export default class Item extends Mdl<ItemProps, any> {
    render(): JSX.Element;
}
