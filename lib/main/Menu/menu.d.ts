import Mdl from "../mdl";
import _Item from "./item";
export interface MenuProps {
    children?: JSX.Element;
    position?: string;
    className?: string;
    ripple?: boolean;
    style?: Object;
    id?: string;
    htmlFor: string;
}
export default class Menu extends Mdl<MenuProps, any> {
    static Item: typeof _Item;
    render(): JSX.Element;
}
