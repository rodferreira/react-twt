"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("../mdl");
var util_1 = require("../util");
var Item = (function (_super) {
    __extends(Item, _super);
    function Item() {
        _super.apply(this, arguments);
    }
    Item.prototype.render = function () {
        var _a = this.props, divider = _a.divider, disabled = _a.disabled, id = _a.id, style = _a.style, children = _a.children;
        var className = "mdl-menu__item";
        if (divider) {
            className += " mdl-menu__item--full-bleed-divider";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("li", {className: className, id: id, style: style, disabled: disabled}, children));
    };
    return Item;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Item;
//# sourceMappingURL=item.js.map