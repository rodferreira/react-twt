"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("../mdl");
var util_1 = require("../util");
var item_1 = require("./item");
var Menu = (function (_super) {
    __extends(Menu, _super);
    function Menu() {
        _super.apply(this, arguments);
    }
    Menu.prototype.render = function () {
        var _a = this.props, position = _a.position, ripple = _a.ripple, style = _a.style, id = _a.id, htmlFor = _a.htmlFor, children = _a.children;
        var className = "mdl-menu mdl-js-menu";
        if (!util_1.isEmpty(position)) {
            className += " mdl-menu--" + position;
        }
        if (ripple) {
            className += " mdl-js-ripple-effect";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("ul", {className: className, style: style, id: id, htmlFor: htmlFor}, children));
    };
    Menu.Item = item_1.default;
    return Menu;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Menu;
//# sourceMappingURL=menu.js.map