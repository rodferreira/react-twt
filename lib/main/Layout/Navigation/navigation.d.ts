import Mdl from "../../mdl";
import _Link from "./link";
export interface NavigationProps {
    id?: string;
    style?: Object;
    className?: string;
    children?: JSX.Element;
    largeScreenOnly?: boolean;
    smallScreenOnly?: boolean;
}
export default class Navigation extends Mdl<NavigationProps, any> {
    static Link: typeof _Link;
    render(): JSX.Element;
}
