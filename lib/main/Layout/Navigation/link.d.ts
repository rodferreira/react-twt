import Mdl from "../../mdl";
export interface LinkProps {
    id?: string;
    style?: Object;
    className?: string;
    children?: JSX.Element;
    href?: string;
    largeScreenOnly?: boolean;
    smallScreenOnly?: boolean;
}
export default class Link extends Mdl<LinkProps, any> {
    render(): JSX.Element;
}
