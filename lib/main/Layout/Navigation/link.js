"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../../util");
var mdl_1 = require("../../mdl");
var Link = (function (_super) {
    __extends(Link, _super);
    function Link() {
        _super.apply(this, arguments);
    }
    Link.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children, largeScreenOnly = _a.largeScreenOnly, smallScreenOnly = _a.smallScreenOnly, href = _a.href;
        var className = "mdl-navigation__link";
        if (largeScreenOnly) {
            className += " mdl-layout--large-screen-only";
        }
        if (smallScreenOnly) {
            className += " mdl-layout--small-screen-only";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("a", {className: className, id: id, style: style, href: href}, children));
    };
    return Link;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Link;
//# sourceMappingURL=link.js.map