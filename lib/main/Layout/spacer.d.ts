import Mdl from "../mdl";
export interface SpacerProps {
    id?: string;
    className?: string;
    style?: Object;
    largeScreenOnly?: boolean;
    smallScreenOnly?: boolean;
}
export default class Spacer extends Mdl<SpacerProps, any> {
    render(): JSX.Element;
}
