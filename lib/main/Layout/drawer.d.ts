import Mdl from "../mdl";
export interface DrawerProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    largeScreenOnly?: boolean;
    smallScreenOnly?: boolean;
}
export default class Header extends Mdl<DrawerProps, any> {
    render(): JSX.Element;
}
