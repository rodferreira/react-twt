"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../../util");
var mdl_1 = require("../../mdl");
var row_1 = require("./row");
var Header = (function (_super) {
    __extends(Header, _super);
    function Header() {
        _super.apply(this, arguments);
    }
    Header.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children, transparent = _a.transparent, scroll = _a.scroll, waterfall = _a.waterfall, waterfallHideTop = _a.waterfallHideTop, layoutIcon = _a.layoutIcon, largeScreenOnly = _a.largeScreenOnly, smallScreenOnly = _a.smallScreenOnly, headerSeamed = _a.headerSeamed;
        var className = "mdl-layout__header";
        if (transparent) {
            className += " mdl-layout__header--transparent";
        }
        if (scroll) {
            className += " mdl-layout__header--scroll";
        }
        if (waterfall) {
            className += " mdl-layout__header--waterfall";
        }
        if (waterfallHideTop) {
            className += " mdl-layout__header--waterfall-hide-top";
        }
        if (largeScreenOnly) {
            className += " mdl-layout--large-screen-only";
        }
        if (smallScreenOnly) {
            className += " mdl-layout--small-screen-only";
        }
        if (headerSeamed) {
            className += " mdl-layout__header--seamed";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("header", {className: className, id: id, style: style}, 
            !util_1.isEmpty(layoutIcon) ?
                React.createElement("img", {class: "mdl-layout-icon", src: layoutIcon})
                :
                    '', 
            children));
    };
    Header.Row = row_1.default;
    return Header;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Header;
//# sourceMappingURL=header.js.map