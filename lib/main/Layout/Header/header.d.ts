import Mdl from "../../mdl";
import _Row from "./row";
export interface HeaderProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    transparent?: boolean;
    scroll?: boolean;
    waterfall?: boolean;
    waterfallHideTop?: boolean;
    layoutIcon?: string;
    largeScreenOnly?: boolean;
    smallScreenOnly?: boolean;
    headerSeamed?: boolean;
}
export default class Header extends Mdl<HeaderProps, any> {
    static Row: typeof _Row;
    render(): JSX.Element;
}
