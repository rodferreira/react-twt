import Mdl from "../../mdl";
export interface RowProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    largeScreenOnly?: boolean;
    smallScreenOnly?: boolean;
}
export default class Row extends Mdl<RowProps, any> {
    render(): JSX.Element;
}
