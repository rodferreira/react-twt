import Mdl from "../mdl";
export interface TitleProps {
    id?: string;
    style?: Object;
    className?: string;
    children?: JSX.Element;
    largeScreenOnly?: boolean;
    smallScreenOnly?: boolean;
}
export default class Title extends Mdl<TitleProps, any> {
    render(): JSX.Element;
}
