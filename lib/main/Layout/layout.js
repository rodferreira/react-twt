"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../util");
var mdl_1 = require("../mdl");
var header_1 = require("./Header/header");
var navigation_1 = require("./Navigation/navigation");
var content_1 = require("./content");
var drawer_1 = require("./drawer");
var spacer_1 = require("./spacer");
var title_1 = require("./title");
var Layout = (function (_super) {
    __extends(Layout, _super);
    function Layout() {
        _super.apply(this, arguments);
    }
    Layout.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children, fixedDrawer = _a.fixedDrawer, fixedHeader = _a.fixedHeader, fixedTabs = _a.fixedTabs, noDrawerButton = _a.noDrawerButton, noDesktopDrawerButton = _a.noDesktopDrawerButton;
        var className = "mdl-layout mdl-js-layout";
        if (fixedDrawer) {
            className += " mdl-layout--fixed-drawer";
        }
        if (fixedHeader) {
            className += " mdl-layout--fixed-header";
        }
        if (fixedTabs) {
            className += " mdl-layout--fixed-tabs";
        }
        if (noDrawerButton) {
            className += " mdl-layout--no-drawer-button";
        }
        if (noDesktopDrawerButton) {
            className += " mdl-layout--no-desktop-drawer-button";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("div", {className: className, style: style, id: id}, children));
    };
    Layout.Header = header_1.default;
    Layout.Navigation = navigation_1.default;
    Layout.Content = content_1.default;
    Layout.Drawer = drawer_1.default;
    Layout.Spacer = spacer_1.default;
    Layout.Title = title_1.default;
    return Layout;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Layout;
//# sourceMappingURL=layout.js.map