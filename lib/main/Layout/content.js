"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../util");
var mdl_1 = require("../mdl");
var Content = (function (_super) {
    __extends(Content, _super);
    function Content() {
        _super.apply(this, arguments);
    }
    Content.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children, largeScreenOnly = _a.largeScreenOnly, smallScreenOnly = _a.smallScreenOnly;
        var className = "mdl-layout__content";
        if (largeScreenOnly) {
            className += " mdl-layout--large-screen-only";
        }
        if (smallScreenOnly) {
            className += " mdl-layout--small-screen-only";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("main", {className: className, id: id, style: style}, children));
    };
    return Content;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Content;
//# sourceMappingURL=content.js.map