import Mdl from "../mdl";
import _Header from "./Header/header";
import _Navigation from "./Navigation/navigation";
import _Content from "./content";
import _Drawer from "./drawer";
import _Spacer from "./spacer";
import _Title from "./title";
export interface LayoutProps {
    id?: string;
    style?: Object;
    className?: string;
    children?: JSX.Element;
    fixedDrawer?: boolean;
    fixedHeader?: boolean;
    fixedTabs?: boolean;
    noDrawerButton?: boolean;
    noDesktopDrawerButton?: boolean;
}
export default class Layout extends Mdl<LayoutProps, any> {
    static Header: typeof _Header;
    static Navigation: typeof _Navigation;
    static Content: typeof _Content;
    static Drawer: typeof _Drawer;
    static Spacer: typeof _Spacer;
    static Title: typeof _Title;
    render(): JSX.Element;
}
