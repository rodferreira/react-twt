import Mdl from "../mdl";
export interface ContentProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    largeScreenOnly?: boolean;
    smallScreenOnly?: boolean;
}
export default class Content extends Mdl<ContentProps, any> {
    render(): JSX.Element;
}
