import Mdl from "./mdl";
export interface TooltipProps {
    htmlFor: string;
    children?: JSX.Element;
    className?: string;
    large?: boolean;
    left?: boolean;
    right?: boolean;
    top?: boolean;
    bottom?: boolean;
}
export default class Tooltip extends Mdl<TooltipProps, any> {
    render(): JSX.Element;
}
