import Mdl from "../mdl";
import _Cell from "./cell";
export interface GridProps {
    id?: string;
    style?: Object;
    className?: string;
    children?: JSX.Element;
}
export default class Grid extends Mdl<GridProps, any> {
    static Cell: typeof _Cell;
    render(): JSX.Element;
}
