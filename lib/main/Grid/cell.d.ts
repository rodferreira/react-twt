import Mdl from "../mdl";
export interface CellProps {
    id?: string;
    style?: Object;
    className?: string;
    children?: JSX.Element;
    devices?: Object;
    col: number;
}
export default class Cell extends Mdl<CellProps, any> {
    render(): JSX.Element;
}
