"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../util");
var mdl_1 = require("../mdl");
var Cell = (function (_super) {
    __extends(Cell, _super);
    function Cell() {
        _super.apply(this, arguments);
    }
    Cell.prototype.render = function () {
        var _a = this.props, col = _a.col, devices = _a.devices, id = _a.id, style = _a.style, children = _a.children;
        var className = "mdl-cell";
        className += " mdl-cell--" + col + "-col";
        if (devices != null) {
            Object.keys(devices).map(function (key) {
                var size = devices[key];
                className += " mdl-cell--" + size + "-col-" + key;
            });
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("div", {className: className, id: id, style: style}, children));
    };
    return Cell;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Cell;
//# sourceMappingURL=cell.js.map