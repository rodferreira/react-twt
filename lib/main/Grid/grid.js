"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../util");
var mdl_1 = require("../mdl");
var cell_1 = require("./cell");
var Grid = (function (_super) {
    __extends(Grid, _super);
    function Grid() {
        _super.apply(this, arguments);
    }
    Grid.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children;
        var className = "mdl-grid";
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("div", {id: id, style: style, className: className}, children));
    };
    Grid.Cell = cell_1.default;
    return Grid;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Grid;
//# sourceMappingURL=grid.js.map