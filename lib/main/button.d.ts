import Mdl from "./mdl";
export interface ButtonProps {
    accent?: boolean;
    children?: JSX.Element;
    className?: string;
    colored?: boolean;
    disabled?: boolean;
    fab?: boolean;
    htmlFor?: string;
    icon?: boolean;
    id?: string;
    miniFab?: boolean;
    onClick?: Function;
    primary?: boolean;
    raised?: boolean;
    ripple?: boolean;
    socialMega?: boolean;
    socialMini?: boolean;
    style?: Object;
}
export default class Button extends Mdl<ButtonProps, any> {
    render(): JSX.Element;
}
