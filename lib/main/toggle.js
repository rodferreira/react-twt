"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var icon_1 = require("./icon");
var util_1 = require("./util");
var mdl_1 = require("./mdl");
var Toggle = (function (_super) {
    __extends(Toggle, _super);
    function Toggle() {
        _super.apply(this, arguments);
    }
    Toggle.prototype.render = function () {
        var _a = this.props, type = _a.type, ripple = _a.ripple, id = _a.id, checked = _a.checked, children = _a.children, onChange = _a.onChange, value = _a.value, name = _a.name;
        var isRadio = type == 'radio';
        var isIcon = type == 'icon';
        var className = "mdl-" + type + (isIcon ? "-toggle" : "") + " mdl-js-" + type + (isIcon ? "-toggle" : "");
        var inputType = isRadio ? "radio" : "checkbox";
        var inputClass = "mdl-" + type + (isIcon ? "-toggle" : "") + "__" + (isRadio ? "button" : "input");
        var labelClass = "mdl-" + type + (isIcon ? "-toggle" : "") + "__label";
        if (ripple) {
            className += " mdl-js-ripple-effect";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("label", {className: className, htmlFor: id}, 
            React.createElement("input", {type: inputType, id: id, className: inputClass, checked: checked, onChange: onChange, value: value, name: name}), 
            isIcon ?
                React.createElement(icon_1.default, {className: "mdl-icon-toggle__label"}, children)
                :
                    React.createElement("span", {className: labelClass}, children)));
    };
    return Toggle;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Toggle;
//# sourceMappingURL=toggle.js.map