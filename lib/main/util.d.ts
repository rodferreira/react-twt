/**
 * ES6 Object.assign
 */
export declare const objectAssign: (target: any, ...sources: any[]) => any;
/**
 * isEmpty function
 */
export declare const isEmpty: (value: any) => boolean;
export declare const isEqual: (a: any, b: any) => boolean;
