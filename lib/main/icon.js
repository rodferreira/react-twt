"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("./util");
var mdl_1 = require("./mdl");
var Icon = (function (_super) {
    __extends(Icon, _super);
    function Icon() {
        _super.apply(this, arguments);
    }
    Icon.prototype.render = function () {
        var _a = this.props, style = _a.style, id = _a.id, children = _a.children, item = _a.item, itemAvatar = _a.itemAvatar;
        var className = "material-icons ";
        if (item) {
            className += " mdl-list__item-icon";
        }
        if (itemAvatar) {
            className += " mdl-list__item-avatar";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("i", {className: className, style: style, id: id}, children));
    };
    return Icon;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Icon;
//# sourceMappingURL=icon.js.map