"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("./mdl");
var util_1 = require("./util");
var Spinner = (function (_super) {
    __extends(Spinner, _super);
    function Spinner() {
        _super.apply(this, arguments);
    }
    Spinner.prototype.render = function () {
        var _a = this.props, isActive = _a.isActive, singleColor = _a.singleColor, id = _a.id, style = _a.style;
        var className = "mdl-spinner mdl-js-spinner";
        if (isActive) {
            className += " is-active";
        }
        if (singleColor) {
            className += " mdl-spinner--single-color";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("p", {id: id, style: style, className: className}));
    };
    return Spinner;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Spinner;
//# sourceMappingURL=spinner.js.map