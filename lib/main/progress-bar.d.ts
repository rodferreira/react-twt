import Mdl from "./mdl";
export interface ProgressBarProps {
    id?: string;
    className?: string;
    style?: Object;
    indeterminate?: boolean;
    progress?: number;
    buffer?: number;
}
export default class ProgressBar extends Mdl<ProgressBarProps, any> {
    private progressBar;
    constructor();
    componentDidMount(): void;
    componentDidUpdate(): void;
    updateProgressStatus(): void;
    render(): JSX.Element;
}
