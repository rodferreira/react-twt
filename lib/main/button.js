"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("./mdl");
var util_1 = require("./util");
var Button = (function (_super) {
    __extends(Button, _super);
    function Button() {
        _super.apply(this, arguments);
    }
    Button.prototype.render = function () {
        var _a = this.props, style = _a.style, id = _a.id, htmlFor = _a.htmlFor, onClick = _a.onClick, children = _a.children, ripple = _a.ripple, fab = _a.fab, miniFab = _a.miniFab, raised = _a.raised, colored = _a.colored, primary = _a.primary, accent = _a.accent, icon = _a.icon, disabled = _a.disabled, socialMega = _a.socialMega, socialMini = _a.socialMini;
        var className = "mdl-button mdl-js-button";
        if (ripple) {
            className += " mdl-js-ripple-effect";
        }
        if (fab) {
            className += " mdl-button--fab";
        }
        if (miniFab) {
            className += " mdl-button--mini-fab";
        }
        if (raised) {
            className += " mdl-button--raised";
        }
        if (colored) {
            className += " mdl-button--colored";
        }
        if (primary) {
            className += " mdl-button--primary";
        }
        if (accent) {
            className += " mdl-button--accent";
        }
        if (icon) {
            className += " mdl-button--icon";
        }
        if (disabled) {
            className += " mdl-button--disabled";
        }
        if (socialMega) {
            className += " mdl-mega-footer__social-btn";
        }
        if (socialMini) {
            className += " mdl-mini-footer__social-btn";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("button", {className: className, style: style, id: id, htmlFor: htmlFor, onClick: function (e) {
            if (onClick != null) {
                onClick(e);
            }
        }}, children));
    };
    return Button;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Button;
//# sourceMappingURL=button.js.map