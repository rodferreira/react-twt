import Mdl from "./mdl";
export interface LogoProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
}
export default class Logo extends Mdl<LogoProps, any> {
    render(): JSX.Element;
}
