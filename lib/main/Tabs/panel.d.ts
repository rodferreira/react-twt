import Mdl from "../mdl";
export interface PanelProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    isActive?: boolean;
    layout?: boolean;
}
export default class Panel extends Mdl<PanelProps, any> {
    render(): JSX.Element;
}
