"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../util");
var mdl_1 = require("../mdl");
var tab_bar_1 = require("./TabBar/tab-bar");
var panel_1 = require("./panel");
var Tabs = (function (_super) {
    __extends(Tabs, _super);
    function Tabs() {
        _super.apply(this, arguments);
    }
    Tabs.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children, ripple = _a.ripple;
        var className = "mdl-tabs mdl-js-tabs";
        if (ripple) {
            className += " mdl-js-ripple-effect";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return React.createElement("div", {className: className, id: id, style: style}, children);
    };
    Tabs.TabBar = tab_bar_1.default;
    Tabs.Panel = panel_1.default;
    return Tabs;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Tabs;
//# sourceMappingURL=tabs.js.map