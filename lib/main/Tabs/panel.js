"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../util");
var mdl_1 = require("../mdl");
var Panel = (function (_super) {
    __extends(Panel, _super);
    function Panel() {
        _super.apply(this, arguments);
    }
    Panel.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children, isActive = _a.isActive, layout = _a.layout;
        var className = layout ? "mdl-layout__tab-panel" : "mdl-tabs__panel";
        if (isActive) {
            className += " is-active";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return React.createElement("div", {className: className, id: id, style: style}, children);
    };
    return Panel;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Panel;
//# sourceMappingURL=panel.js.map