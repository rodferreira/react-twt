import Mdl from "../mdl";
import _TabBar from "./TabBar/tab-bar";
import _Panel from "./panel";
export interface TabsProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    ripple?: boolean;
}
export default class Tabs extends Mdl<TabsProps, any> {
    static TabBar: typeof _TabBar;
    static Panel: typeof _Panel;
    render(): JSX.Element;
}
