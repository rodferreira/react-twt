import Mdl from "../../mdl";
import _Tab from "./tab";
export interface TabBarProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    layout?: boolean;
    ripple?: boolean;
}
export default class TabBar extends Mdl<TabBarProps, any> {
    static Tab: typeof _Tab;
    render(): JSX.Element;
}
