import Mdl from "../../mdl";
export interface TabProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    href?: string;
    isActive?: boolean;
    layout?: boolean;
}
export default class Tab extends Mdl<TabProps, any> {
    render(): JSX.Element;
}
