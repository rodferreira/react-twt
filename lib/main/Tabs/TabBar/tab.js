"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../../util");
var mdl_1 = require("../../mdl");
var Tab = (function (_super) {
    __extends(Tab, _super);
    function Tab() {
        _super.apply(this, arguments);
    }
    Tab.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children, isActive = _a.isActive, href = _a.href, layout = _a.layout;
        var className = layout ? "mdl-layout__tab" : "mdl-tabs__tab";
        if (isActive) {
            className += " is-active";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return React.createElement("a", {href: href, className: className, id: id, style: style}, children);
    };
    return Tab;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Tab;
//# sourceMappingURL=tab.js.map