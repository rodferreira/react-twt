"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../../util");
var mdl_1 = require("../../mdl");
var tab_1 = require("./tab");
var TabBar = (function (_super) {
    __extends(TabBar, _super);
    function TabBar() {
        _super.apply(this, arguments);
    }
    TabBar.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children, layout = _a.layout, ripple = _a.ripple;
        var className = layout ? "mdl-layout__tab-bar" : "mdl-tabs__tab-bar";
        if (ripple) {
            className += " mdl-js-ripple-effect";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return React.createElement("div", {className: className, id: id, style: style}, children);
    };
    TabBar.Tab = tab_1.default;
    return TabBar;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TabBar;
//# sourceMappingURL=tab-bar.js.map