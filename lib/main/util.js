"use strict";
/**
 * ES6 Object.assign
 */
exports.objectAssign = function (target) {
    'use strict';
    var sources = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        sources[_i - 1] = arguments[_i];
    }
    if (target === undefined || target === null) {
        throw new TypeError('Cannot convert undefined or null to object');
    }
    var output = Object(target);
    for (var index = 1; index < arguments.length; index++) {
        var source = arguments[index];
        if (source !== undefined && source !== null) {
            for (var nextKey in source) {
                if (source.hasOwnProperty(nextKey)) {
                    output[nextKey] = source[nextKey];
                }
            }
        }
    }
    return output;
};
/**
 * isEmpty function
 */
exports.isEmpty = function (value) {
    if (value == null) {
        return true;
    }
    switch (typeof value) {
        case "string":
            return value.trim().length == 0;
        case "object":
            return value.length == 0;
    }
};
exports.isEqual = function (a, b) {
    var t1 = typeof a;
    var t2 = typeof b;
    if (t1 != t2) {
        return false;
    }
    if (t1 == 'object') {
        if (a == null && b == null) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }
        var equal_1 = true;
        if (a instanceof Array) {
            if (a.length != b.length) {
                equal_1 = false;
            }
            else {
                a.forEach(function (value, i) {
                    if (!exports.isEqual(value, b[i])) {
                        equal_1 = false;
                    }
                });
            }
        }
        else {
            Object.keys(a).forEach(function (key) {
                var v1 = a[key];
                var v2 = b[key];
                var tk1 = typeof v1;
                var tk2 = typeof v2;
                if (tk1 != tk2) {
                    equal_1 = false;
                }
                else if (tk1 == 'object') {
                    if (!exports.isEqual(v1, v2)) {
                        equal_1 = false;
                    }
                }
                else {
                    if (v1 !== v2) {
                        equal_1 = false;
                    }
                }
            });
        }
        return equal_1;
    }
    else {
        return a === b;
    }
};
//# sourceMappingURL=util.js.map