import Mdl from "./mdl";
export interface SliderProps {
    id?: string;
    className?: string;
    style?: Object;
    min?: number;
    max?: number;
    value?: number;
    step?: number;
    disabled?: boolean;
    onChange?: Function;
}
export default class Slider extends Mdl<SliderProps, any> {
    render(): JSX.Element;
}
