import Mdl from "../mdl";
export interface MenuProps {
    children?: JSX.Element;
    id?: string;
    border?: boolean;
    className?: string;
    style?: Object;
}
export default class Menu extends Mdl<MenuProps, any> {
    render(): JSX.Element;
}
