import Mdl from "../mdl";
export interface SupportingTextProps {
    children?: JSX.Element;
    id?: string;
    border?: boolean;
    className?: string;
    style?: Object;
}
export default class SupportingText extends Mdl<SupportingTextProps, any> {
    render(): JSX.Element;
}
