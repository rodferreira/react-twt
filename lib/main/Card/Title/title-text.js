"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("../../mdl");
var util_1 = require("../../util");
var Title = (function (_super) {
    __extends(Title, _super);
    function Title() {
        _super.apply(this, arguments);
    }
    Title.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children;
        var className = "mdl-card__title-text";
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("h2", {className: className, id: id, style: style}, children));
    };
    return Title;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Title;
//# sourceMappingURL=title-text.js.map