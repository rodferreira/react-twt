import Mdl from "../../mdl";
import _TitleText from "./title-text";
import _SubtitleText from "./subtitle-text";
export interface TitleProps {
    children?: JSX.Element;
    expand?: boolean;
    id?: string;
    border?: boolean;
    className?: string;
    style?: Object;
}
export default class Title extends Mdl<TitleProps, any> {
    static Text: typeof _TitleText;
    static Subtitle: typeof _SubtitleText;
    render(): JSX.Element;
}
