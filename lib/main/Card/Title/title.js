"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("../../mdl");
var util_1 = require("../../util");
var title_text_1 = require("./title-text");
var subtitle_text_1 = require("./subtitle-text");
var Title = (function (_super) {
    __extends(Title, _super);
    function Title() {
        _super.apply(this, arguments);
    }
    Title.prototype.render = function () {
        var _a = this.props, border = _a.border, id = _a.id, style = _a.style, children = _a.children, expand = _a.expand;
        var className = "mdl-card__title";
        if (expand) {
            className += " mdl-card--expand";
        }
        if (border) {
            className += " mdl-card--border";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("div", {className: className, id: id, style: style}, children));
    };
    Title.Text = title_text_1.default;
    Title.Subtitle = subtitle_text_1.default;
    return Title;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Title;
//# sourceMappingURL=title.js.map