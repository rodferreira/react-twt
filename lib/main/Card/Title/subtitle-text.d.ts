import Mdl from "../../mdl";
export interface SubtitleTextProps {
    children?: JSX.Element;
    id?: string;
    className?: string;
    style?: Object;
}
export default class SubtitleText extends Mdl<SubtitleTextProps, any> {
    render(): JSX.Element;
}
