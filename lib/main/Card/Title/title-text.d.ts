import Mdl from "../../mdl";
export interface TitleTextProps {
    children?: JSX.Element;
    id?: string;
    className?: string;
    style?: Object;
}
export default class Title extends Mdl<TitleTextProps, any> {
    render(): JSX.Element;
}
