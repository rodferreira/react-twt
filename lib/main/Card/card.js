"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("../mdl");
var util_1 = require("../util");
var actions_1 = require("./actions");
var media_1 = require("./media");
var title_1 = require("./Title/title");
var supporting_text_1 = require("./supporting-text");
var menu_1 = require("./menu");
var Card = (function (_super) {
    __extends(Card, _super);
    function Card() {
        _super.apply(this, arguments);
    }
    Card.prototype.render = function () {
        var _a = this.props, style = _a.style, id = _a.id, children = _a.children, shadow = _a.shadow;
        var className = "mdl-card";
        if (shadow != null) {
            className += " mdl-shadow--" + shadow + "dp";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("div", {className: className, style: style, id: id}, children));
    };
    Card.Actions = actions_1.default;
    Card.Media = media_1.default;
    Card.Title = title_1.default;
    Card.SupportingText = supporting_text_1.default;
    Card.Menu = menu_1.default;
    return Card;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Card;
//# sourceMappingURL=card.js.map