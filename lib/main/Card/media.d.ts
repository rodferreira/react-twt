import Mdl from "../mdl";
export interface MediaProps {
    children?: JSX.Element;
    id?: string;
    border?: boolean;
    className?: string;
    style?: Object;
}
export default class Media extends Mdl<MediaProps, any> {
    render(): JSX.Element;
}
