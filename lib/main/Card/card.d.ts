import Mdl from "../mdl";
import _Actions from "./actions";
import _Media from "./media";
import _Title from "./Title/title";
import _SupportingText from "./supporting-text";
import _Menu from "./menu";
export interface CardProps {
    children?: JSX.Element;
    shadow?: number;
    className?: string;
    style?: Object;
    id?: string;
}
export default class Card extends Mdl<CardProps, any> {
    static Actions: typeof _Actions;
    static Media: typeof _Media;
    static Title: typeof _Title;
    static SupportingText: typeof _SupportingText;
    static Menu: typeof _Menu;
    render(): JSX.Element;
}
