import Mdl from "../mdl";
export interface ActionsProps {
    children?: JSX.Element;
    id?: string;
    border?: boolean;
    className?: string;
    style?: Object;
}
export default class Actions extends Mdl<ActionsProps, any> {
    render(): JSX.Element;
}
