import Mdl from "./mdl";
export interface ToggleProps {
    id: string;
    type?: string;
    ripple?: boolean;
    onChange?: Function;
    className?: string;
    checked?: boolean;
    children?: JSX.Element;
    name?: string;
    value?: string;
}
export default class Toggle extends Mdl<ToggleProps, any> {
    render(): JSX.Element;
}
