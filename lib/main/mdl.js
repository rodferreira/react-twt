"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var ReactDom = require("react-dom");
var Mdl = (function (_super) {
    __extends(Mdl, _super);
    function Mdl() {
        _super.apply(this, arguments);
    }
    Mdl.prototype.componentDidMount = function () {
        //componentHandler.upgradeElement(ReactDom.findDOMNode(this));
        componentHandler.upgradeDom();
    };
    Mdl.prototype.componentWillUnmount = function () {
        componentHandler.downgradeElements(ReactDom.findDOMNode(this));
    };
    Mdl.prototype.componentDidUpdate = function () {
        //componentHandler.upgradeElement(ReactDom.findDOMNode(this));
        componentHandler.upgradeDom();
    };
    return Mdl;
}(React.Component));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Mdl;
//# sourceMappingURL=mdl.js.map