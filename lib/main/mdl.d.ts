import * as React from "react";
declare abstract class Mdl<P, S> extends React.Component<P, S> {
    componentDidMount(): void;
    componentWillUnmount(): void;
    componentDidUpdate(): void;
}
export default Mdl;
