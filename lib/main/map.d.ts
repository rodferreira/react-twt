import * as React from "react";
export interface MapProps {
    id?: string;
    className?: string;
    style?: Object;
    center?: google.maps.LatLng;
    zoom?: number;
    layers?: Object;
    onZoomChanged?: Function;
    onBoundsChanged?: Function;
    hiddenLayers?: Array<string>;
    bbox?: string;
}
export default class Map extends React.Component<MapProps, any> {
    private map;
    private layers;
    private arrowIcon;
    private infoWindow;
    private points;
    constructor();
    componentDidMount(): void;
    componentWillUnmount(): void;
    componentDidUpdate(prevProps: any, prevState: any): void;
    private updateLayer(name);
    private hideLayer(name);
    private showLayer(name);
    private addMarker(feature, hiddenLayer);
    private addWkt(wkt, color, arrow, hiddenLayer);
    private createMap();
    private bboxToBounds(bbox);
    private boundsToBbox(bounds);
    private createArrowIcon();
    private destroyMap();
    render(): JSX.Element;
}
