import Mdl from "./mdl";
export interface IconProps {
    style?: Object;
    className?: string;
    id?: string;
    item?: boolean;
    itemAvatar?: boolean;
}
export default class Icon extends Mdl<any, any> {
    render(): JSX.Element;
}
