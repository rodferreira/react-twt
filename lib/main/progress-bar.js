"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var ReactDom = require("react-dom");
var mdl_1 = require("./mdl");
var util_1 = require("./util");
var ProgressBar = (function (_super) {
    __extends(ProgressBar, _super);
    function ProgressBar() {
        _super.call(this);
        this.progressBar = null;
    }
    ProgressBar.prototype.componentDidMount = function () {
        _super.prototype.componentDidMount.call(this);
        this.progressBar = ReactDom.findDOMNode(this);
        this.updateProgressStatus();
    };
    ProgressBar.prototype.componentDidUpdate = function () {
        _super.prototype.componentDidUpdate.call(this);
        this.updateProgressStatus();
    };
    ProgressBar.prototype.updateProgressStatus = function () {
        if (this.props.progress != null) {
            this.progressBar.MaterialProgress.setProgress(this.props.progress);
        }
        if (this.props.buffer != null) {
            this.progressBar.MaterialProgress.setBuffer(this.props.buffer);
        }
    };
    ProgressBar.prototype.render = function () {
        var _a = this.props, indeterminate = _a.indeterminate, id = _a.id, style = _a.style;
        var className = "mdl-progress mdl-js-progress";
        if (indeterminate) {
            className += " mdl-progress__indeterminate";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("div", {id: id, className: className, style: style}));
    };
    return ProgressBar;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = ProgressBar;
//# sourceMappingURL=progress-bar.js.map