import Mdl from "./mdl";
export interface SnackbarProps {
    id?: string;
    style?: Object;
    className?: string;
    message?: String;
    timeout?: number;
    actionHandler?: Function;
    actionText?: string;
    show?: boolean;
}
export default class Snackbar extends Mdl<SnackbarProps, any> {
    private snackbar;
    constructor();
    componentDidMount(): void;
    componentDidUpdate(): void;
    private showSnackBar();
    render(): JSX.Element;
}
