"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("../mdl");
var util_1 = require("../util");
var item_1 = require("./Item/item");
var List = (function (_super) {
    __extends(List, _super);
    function List() {
        _super.apply(this, arguments);
    }
    List.prototype.render = function () {
        var _a = this.props, children = _a.children, id = _a.id, style = _a.style;
        var className = "mdl-list";
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("ul", {className: className, id: id, style: style}, children));
    };
    List.Item = item_1.default;
    return List;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = List;
//# sourceMappingURL=list.js.map