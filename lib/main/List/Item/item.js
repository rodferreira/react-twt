"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../../util");
var mdl_1 = require("../../mdl");
var content_1 = require("./Content/content");
var subtitle_1 = require("./subtitle");
var Item = (function (_super) {
    __extends(Item, _super);
    function Item() {
        _super.apply(this, arguments);
    }
    Item.prototype.render = function () {
        var _a = this.props, twoLine = _a.twoLine, threeLine = _a.threeLine, style = _a.style, id = _a.id, children = _a.children, onClick = _a.onClick;
        var className = "mdl-list__item";
        if (twoLine) {
            className += " mdl-list__item--two-line";
        }
        if (threeLine) {
            className += " mdl-list__item--three-line";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("li", {className: className, id: id, style: style, onClick: onClick}, children));
    };
    Item.Content = content_1.default;
    Item.Subtitle = subtitle_1.default;
    return Item;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Item;
//# sourceMappingURL=item.js.map