import Mdl from "../../mdl";
export interface SubtitleProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
}
export default class Subtitle extends Mdl<SubtitleProps, any> {
    render(): JSX.Element;
}
