import Mdl from "../../mdl";
import _Content from "./Content/content";
import _Subtitle from "./subtitle";
export interface ItemProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    twoLine?: boolean;
    threeLine?: boolean;
    onClick?: Function;
}
export default class Item extends Mdl<ItemProps, any> {
    static Content: typeof _Content;
    static Subtitle: typeof _Subtitle;
    render(): JSX.Element;
}
