"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../../../util");
var mdl_1 = require("../../../mdl");
var action_1 = require("./action");
var text_body_1 = require("./text-body");
var info_1 = require("./info");
var Item = (function (_super) {
    __extends(Item, _super);
    function Item() {
        _super.apply(this, arguments);
    }
    Item.prototype.render = function () {
        var _a = this.props, secondary = _a.secondary, id = _a.id, style = _a.style, children = _a.children;
        var className = "mdl-list__item-" + (secondary ? "secondary" : "primary") + "-content";
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("span", {className: className, id: id, style: style}, children));
    };
    Item.Action = action_1.default;
    Item.TextBody = text_body_1.default;
    Item.Info = info_1.default;
    return Item;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Item;
//# sourceMappingURL=content.js.map