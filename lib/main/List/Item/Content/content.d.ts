import Mdl from "../../../mdl";
import _Action from "./action";
import _TextBody from "./text-body";
import _Info from "./info";
export interface ItemProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    secondary?: boolean;
}
export default class Item extends Mdl<ItemProps, any> {
    static Action: typeof _Action;
    static TextBody: typeof _TextBody;
    static Info: typeof _Info;
    render(): JSX.Element;
}
