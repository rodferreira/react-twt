"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../../../util");
var mdl_1 = require("../../../mdl");
var Info = (function (_super) {
    __extends(Info, _super);
    function Info() {
        _super.apply(this, arguments);
    }
    Info.prototype.render = function () {
        var _a = this.props, secondary = _a.secondary, id = _a.id, style = _a.style, children = _a.children;
        var className = "mdl-list__item-" + (secondary ? "secondary" : "primary") + "-info";
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("span", {className: className, id: id, style: style}, children));
    };
    return Info;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Info;
//# sourceMappingURL=info.js.map