import Mdl from "../../../mdl";
export interface ItemProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    secondary?: boolean;
    anchor?: boolean;
    href?: string;
    onClick?: Function;
}
export default class Item extends Mdl<ItemProps, any> {
    render(): JSX.Element;
}
