import Mdl from "../../../mdl";
export interface InfoProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    secondary?: boolean;
}
export default class Info extends Mdl<InfoProps, any> {
    render(): JSX.Element;
}
