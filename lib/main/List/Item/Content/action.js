"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var util_1 = require("../../../util");
var mdl_1 = require("../../../mdl");
var Item = (function (_super) {
    __extends(Item, _super);
    function Item() {
        _super.apply(this, arguments);
    }
    Item.prototype.render = function () {
        var _a = this.props, secondary = _a.secondary, id = _a.id, style = _a.style, children = _a.children, anchor = _a.anchor, onClick = _a.onClick, href = _a.href;
        var className = "mdl-list__item-" + (secondary ? "secondary" : "primary") + "-action";
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        if (anchor) {
            return React.createElement("a", {className: className, id: id, style: style, onClick: onClick, href: href}, children);
        }
        return React.createElement("span", {className: className, id: id, style: style, onClick: onClick}, children);
    };
    return Item;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Item;
//# sourceMappingURL=action.js.map