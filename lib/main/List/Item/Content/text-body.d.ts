import Mdl from "../../../mdl";
export interface TextBodyProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
}
export default class TextBody extends Mdl<TextBodyProps, any> {
    render(): JSX.Element;
}
