import Mdl from "../mdl";
import _Item from "./Item/item";
export interface ListProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
}
export default class List extends Mdl<ListProps, any> {
    static Item: typeof _Item;
    render(): JSX.Element;
}
