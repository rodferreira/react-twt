"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var ReactDom = require("react-dom");
var util_1 = require("./util");
var TWTMarker = (function (_super) {
    __extends(TWTMarker, _super);
    function TWTMarker(markerOpts) {
        _super.call(this, markerOpts);
    }
    return TWTMarker;
}(google.maps.Marker));
var Map = (function (_super) {
    __extends(Map, _super);
    function Map() {
        _super.call(this);
        this.map = null;
    }
    Map.prototype.componentDidMount = function () {
        console.log('mounted');
        this.createMap();
        this.createArrowIcon();
        this.infoWindow = new google.maps.InfoWindow();
        if (this.props.layers != null) {
            for (var key in this.props.layers) {
                this.updateLayer(key);
            }
        }
    };
    Map.prototype.componentWillUnmount = function () {
        console.log('will unmount');
        this.destroyMap();
    };
    Map.prototype.componentDidUpdate = function (prevProps, prevState) {
        var _this = this;
        console.log("componentDidUpdate");
        var _a = this.props, layers = _a.layers, hiddenLayers = _a.hiddenLayers, zoom = _a.zoom, bbox = _a.bbox;
        var oldLayers = prevProps.layers;
        var oldHidden = prevProps.hiddenLayers;
        Object.keys(layers).forEach(function (key) {
            if (!util_1.isEqual(layers[key], oldLayers[key])) {
                _this.updateLayer(key);
            }
        });
        if (!util_1.isEqual(oldHidden, hiddenLayers)) {
            Object.keys(layers).forEach(function (key) {
                if (oldHidden.indexOf(key) == -1 && hiddenLayers.indexOf(key) != -1) {
                    _this.hideLayer(key);
                }
                else if (oldHidden.indexOf(key) != -1 && hiddenLayers.indexOf(key) == -1) {
                    _this.showLayer(key);
                }
            });
        }
        if (!util_1.isEqual(prevProps.zoom, zoom)) {
            if (zoom != this.map.getZoom()) {
                this.map.setZoom(zoom);
            }
        }
        if (!util_1.isEqual(prevProps.bbox, bbox)) {
            var mapBbox = this.boundsToBbox(this.map.getBounds());
            var newBounds = this.bboxToBounds(bbox);
            if (bbox != mapBbox) {
                this.map.panToBounds(newBounds);
            }
        }
    };
    Map.prototype.updateLayer = function (name) {
        var _this = this;
        var hiddenLayers = this.props.hiddenLayers;
        this.hideLayer(name);
        this.layers[name] = [];
        var features = this.props.layers[name];
        var hiddenLayer = hiddenLayers == null ? false : hiddenLayers.indexOf(name) != -1;
        if (features != null) {
            features.forEach(function (feature) {
                if (feature.wkt != null) {
                    var geos = _this.addWkt(feature.wkt, feature.color, feature.arrow, hiddenLayer);
                    geos.forEach(function (geo) {
                        _this.layers[name].push(geo);
                    });
                }
                else {
                    var marker = _this.addMarker(feature, hiddenLayer);
                    _this.layers[name].push(marker);
                }
            });
        }
    };
    Map.prototype.hideLayer = function (name) {
        if (this.layers[name] !== undefined) {
            this.layers[name].forEach(function (feature) {
                feature.setMap(null);
            });
        }
    };
    Map.prototype.showLayer = function (name) {
        var _this = this;
        var features = this.layers[name];
        if (features != null) {
            features.forEach(function (feature) {
                feature.setMap(_this.map);
            });
        }
    };
    Map.prototype.addMarker = function (feature, hiddenLayer) {
        var _this = this;
        var image = {
            url: feature.img,
            origin: new google.maps.Point(0, 0)
        };
        if (feature.w != null && feature.h != null) {
            image['scaledSize'] = new google.maps.Size(feature.w, feature.h);
        }
        if (feature.aw != null && feature.ah != null) {
            image['anchor'] = new google.maps.Point(feature.aw, feature.ah);
        }
        var marker = new TWTMarker({
            position: new google.maps.LatLng(feature.lat, feature.lon),
            icon: image,
            map: hiddenLayer ? null : this.map,
            title: feature.title,
            info: feature.info
        });
        if (feature.info != null) {
            google.maps.event.addListener(marker, 'click', function () {
                _this.infoWindow.setContent(feature.info);
                _this.infoWindow.setPosition(marker.getPosition());
                _this.infoWindow.open(marker.getMap());
            });
        }
        return marker;
    };
    Map.prototype.addWkt = function (wkt, color, arrow, hiddenLayer) {
        var _this = this;
        if (wkt != null && wkt.trim().length != 0) {
            var typeRegex = /[A-Z]+/;
            var elemRegex = /\(((\s+)?\-?[0-9]+(\.[0-9]*)?\s+\-?[0-9]+(\.[0-9]*)?(\s+)?\,?)+\)/g;
            var coordRegex_1 = /(\-?[0-9]+(\.[0-9]*)?)\s+(\-?[0-9]+(\.[0-9]*)?)/g;
            var type_1 = wkt.match(typeRegex)[0].toLowerCase();
            var elems = wkt.match(elemRegex);
            var geos_1 = [];
            elems.forEach(function (elem) {
                var ptsArray = [];
                var coords = null;
                while ((coords = coordRegex_1.exec(elem)) != null) {
                    ptsArray.push(new google.maps.LatLng(parseFloat(coords[3]), parseFloat(coords[1])));
                }
                var geoOpts = {
                    path: ptsArray,
                    strokeColor: color,
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: color,
                    fillOpacity: 0.35,
                    //radius: 100,
                    center: ptsArray[0],
                    radius: Math.pow(2, (21 - _this.map.getZoom())) * 140 * 0.0027,
                    type: type_1
                };
                if (arrow) {
                    geoOpts['icons'] = [_this.arrowIcon];
                }
                var geo = null;
                if (type_1.indexOf('line') != -1) {
                    geo = new google.maps.Polyline(geoOpts);
                }
                else if (type_1.indexOf('point') != -1) {
                    geo = new google.maps.Circle(geoOpts);
                }
                else {
                    geo = new google.maps.Polygon(geoOpts);
                }
                if (!hiddenLayer) {
                    geo.setMap(_this.map);
                }
                geos_1.push(geo);
            });
            return geos_1;
        }
    };
    Map.prototype.createMap = function () {
        var _this = this;
        console.log('create Map');
        this.layers = [];
        var _a = this.props, center = _a.center, zoom = _a.zoom, onZoomChanged = _a.onZoomChanged, onBoundsChanged = _a.onBoundsChanged, bbox = _a.bbox;
        this.map = new google.maps.Map(ReactDom.findDOMNode(this), {
            center: this.props.center,
            zoom: this.props.zoom
        });
        google.maps.event.addListener(this.map, 'zoom_changed', function () {
            for (var key in _this.layers) {
                _this.layers[key].forEach(function (feature) {
                    if (feature.type == 'point') {
                        var p = Math.pow(2, (21 - _this.map.getZoom()));
                        feature.setRadius(p * 140 * 0.0027);
                    }
                });
            }
        });
        if (onZoomChanged !== undefined) {
            google.maps.event.addListener(this.map, 'zoom_changed', function () {
                onZoomChanged(_this.map.getZoom());
            });
        }
        if (onBoundsChanged !== undefined) {
            google.maps.event.addListener(this.map, 'bounds_changed', function () {
                onBoundsChanged(_this.boundsToBbox(_this.map.getBounds()));
            });
        }
        if (bbox != null) {
            this.map.panToBounds(this.bboxToBounds(bbox));
        }
    };
    Map.prototype.bboxToBounds = function (bbox) {
        var values = bbox.split(",");
        var sw = new google.maps.LatLng(parseFloat(values[1]), parseFloat(values[0]));
        var ne = new google.maps.LatLng(parseFloat(values[3]), parseFloat(values[2]));
        return new google.maps.LatLngBounds(sw, ne);
    };
    Map.prototype.boundsToBbox = function (bounds) {
        var sw = bounds.getSouthWest();
        var ne = bounds.getNorthEast();
        return sw.lng() + "," + sw.lat() + "," + ne.lng() + "," + ne.lat();
    };
    Map.prototype.createArrowIcon = function () {
        this.arrowIcon = {
            icon: {
                path: 'M0 -3 L3 4 L0 2 L-3 4 Z',
                strokeColor: '#333',
                fillColor: '#fff',
                fillOpacity: 0.5
            },
            fillColor: 'black',
            repeat: '300px'
        };
    };
    Map.prototype.destroyMap = function () {
        console.log('destroy map');
        //this.map.setTarget(null);
        this.map = null;
    };
    Map.prototype.render = function () {
        var _a = this.props, id = _a.id, className = _a.className, style = _a.style;
        return (React.createElement("div", {id: id, className: className, style: style}));
    };
    return Map;
}(React.Component));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Map;
//# sourceMappingURL=map.js.map