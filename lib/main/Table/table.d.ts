import Mdl from "../mdl";
import _Th from "./th";
import _Td from "./td";
export interface TableProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    shadow?: number;
}
export default class Table extends Mdl<TableProps, any> {
    static Th: typeof _Th;
    static Td: typeof _Td;
    render(): JSX.Element;
}
