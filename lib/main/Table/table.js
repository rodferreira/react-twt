"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("../mdl");
var util_1 = require("../util");
var th_1 = require("./th");
var td_1 = require("./td");
var Table = (function (_super) {
    __extends(Table, _super);
    function Table() {
        _super.apply(this, arguments);
    }
    Table.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children, shadow = _a.shadow;
        var className = "mdl-data-table mdl-js-data-table";
        if (shadow != null) {
            className += " mdl-shadow--" + shadow + "dp";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("table", {className: className, id: id, style: style}, children));
    };
    Table.Th = th_1.default;
    Table.Td = td_1.default;
    return Table;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Table;
//# sourceMappingURL=table.js.map