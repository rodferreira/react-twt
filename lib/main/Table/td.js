"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("../mdl");
var util_1 = require("../util");
var Th = (function (_super) {
    __extends(Th, _super);
    function Th() {
        _super.apply(this, arguments);
    }
    Th.prototype.render = function () {
        var _a = this.props, id = _a.id, style = _a.style, children = _a.children, nonNumeric = _a.nonNumeric;
        var className = "";
        if (nonNumeric) {
            className += " mdl-data-table__cell--non-numeric";
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("td", {className: className, id: id, style: style}, children));
    };
    return Th;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Th;
//# sourceMappingURL=td.js.map