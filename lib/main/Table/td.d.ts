import Mdl from "../mdl";
export interface ThProps {
    id?: string;
    className?: string;
    style?: Object;
    children?: JSX.Element;
    nonNumeric?: boolean;
}
export default class Th extends Mdl<ThProps, any> {
    render(): JSX.Element;
}
