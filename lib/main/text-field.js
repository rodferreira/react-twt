"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var ReactDom = require("react-dom");
var mdl_1 = require("./mdl");
var icon_1 = require("./icon");
var util_1 = require("./util");
var TextField = (function (_super) {
    __extends(TextField, _super);
    function TextField() {
        _super.apply(this, arguments);
    }
    TextField.prototype.componentDidMount = function () {
        if (this.props.multiLine) {
            this.fixTextArea();
        }
        _super.prototype.componentDidMount.call(this);
    };
    TextField.prototype.componentDidUpdate = function () {
        if (this.props.multiLine) {
            this.fixTextArea();
        }
        _super.prototype.componentDidUpdate.call(this);
    };
    TextField.prototype.fixTextArea = function () {
        var compDom = ReactDom.findDOMNode(this);
        var textAreas = compDom.getElementsByTagName('textarea');
        if (this.props.maxRows != null) {
            textAreas[0].setAttribute('maxrows', this.props.maxRows.toString());
        }
    };
    TextField.prototype.buildMainGroup = function () {
        var _a = this.props, icon = _a.icon, floatingLabel = _a.floatingLabel, isInvalid = _a.isInvalid, type = _a.type, id = _a.id, pattern = _a.pattern, value = _a.value, onChange = _a.onChange, rows = _a.rows, disabled = _a.disabled, label = _a.label, error = _a.error, multiLine = _a.multiLine, alignRight = _a.alignRight;
        var className = "mdl-textfield mdl-js-textfield";
        if (!util_1.isEmpty(icon)) {
            className = "mdl-textfield__expandable-holder";
        }
        else {
            if (floatingLabel) {
                className += " mdl-textfield--floating-label";
            }
            if (isInvalid) {
                className += " is-invalid";
            }
            if (alignRight) {
                className += " mdl-textfield--align-right";
            }
        }
        if (!util_1.isEmpty(this.props.className)) {
            className = this.props.className + " " + className;
        }
        return (React.createElement("div", {className: className}, 
            !multiLine ?
                React.createElement("input", {className: "mdl-textfield__input", type: type || "text", id: id, pattern: pattern, value: value, onChange: onChange, disabled: disabled})
                :
                    React.createElement("textarea", {className: "mdl-textfield__input", type: type, rows: rows, id: id, pattern: pattern, value: value, onChange: onChange, disabled: disabled}), 
            React.createElement("label", {className: "mdl-textfield__label", htmlFor: id}, label), 
            error ?
                React.createElement("span", {className: "mdl-textfield__error"}, error)
                :
                    ''));
    };
    TextField.prototype.render = function () {
        var _a = this.props, icon = _a.icon, isInvalid = _a.isInvalid, id = _a.id, alignRight = _a.alignRight;
        var mainGroup = this.buildMainGroup();
        var result = null;
        if (util_1.isEmpty(icon)) {
            result = mainGroup;
        }
        else {
            result = React.createElement("div", {className: "mdl-textfield mdl-js-textfield mdl-textfield--expandable" + (isInvalid ? " is-invalid" : "") + (alignRight ? " mdl-textfield--align-right" : "")}, 
                React.createElement("label", {className: "mdl-button mdl-js-button mdl-button--icon", htmlFor: id}, 
                    React.createElement(icon_1.default, null, icon)
                ), 
                mainGroup);
        }
        return result;
    };
    return TextField;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = TextField;
//# sourceMappingURL=text-field.js.map