import Mdl from "./mdl";
export interface BadgeProps {
    children?: JSX.Element;
    className?: string;
    data?: string;
    href?: string;
    icon?: boolean;
    id?: string;
    noBackground?: boolean;
    onClick?: Function;
    overlap?: boolean;
    style?: Object;
    tagName?: string;
}
export default class Badge extends Mdl<BadgeProps, any> {
    render(): JSX.Element;
}
