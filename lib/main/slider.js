"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var React = require("react");
var mdl_1 = require("./mdl");
var Slider = (function (_super) {
    __extends(Slider, _super);
    function Slider() {
        _super.apply(this, arguments);
    }
    Slider.prototype.render = function () {
        var _a = this.props, style = _a.style, id = _a.id, className = _a.className, min = _a.min, max = _a.max, value = _a.value, step = _a.step, disabled = _a.disabled, onChange = _a.onChange;
        return (React.createElement("p", {style: style, className: className}, 
            React.createElement("input", {className: "mdl-slider mdl-js-slider", type: "range", id: id, min: min, max: max, value: value, step: step, disabled: disabled, onChange: onChange})
        ));
    };
    return Slider;
}(mdl_1.default));
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = Slider;
//# sourceMappingURL=slider.js.map