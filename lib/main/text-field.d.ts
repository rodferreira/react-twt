import Mdl from "./mdl";
export interface TextFieldProps {
    id?: string;
    multiLine?: boolean;
    rows?: number;
    pattern?: string;
    error?: string;
    label?: string;
    floatingLabel?: boolean;
    maxRows?: number;
    icon?: string;
    isInvalid?: boolean;
    value?: string;
    type?: string;
    onChange?: Function;
    disabled?: boolean;
    alignRight?: boolean;
    className?: string;
}
export default class TextField extends Mdl<TextFieldProps, any> {
    componentDidMount(): void;
    componentDidUpdate(): void;
    private fixTextArea();
    private buildMainGroup();
    render(): JSX.Element;
}
