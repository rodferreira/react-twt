/*
*	Task Automation to make my life easier.
*	Author: Jean-Pierre Sierens
*   Modified by: Rodrigo Ferreira (rodrigof@tecgraf.puc-rio.br)
*	===========================================================================
*/

// declarations, dependencies
// ----------------------------------------------------------------------------
var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var gutil = require('gulp-util');
var babelify = require('babelify');
var mocha = require('gulp-mocha');
var tsify = require("tsify");

// External dependencies you do not want to rebundle while developing,
// but include in your application deployment
var dependencies = [
    'react',
    'react-dom',
    'redux',
    'react-redux',
    'redux-thunk'
];
var testDependencies = [
    'expect',
    'deep-freeze'
];

gulp.task('test', function () {
    bundleTest();
});

gulp.task('watchTest', function () {
    gulp.watch(['./src/main/**/*.tsx', './src/test/**/*.tsx'], ['test']);
});

// When running 'gulp' on the terminal this task will fire.
// It will start watching for changes in every .js file.
// If there's a change, the task 'scripts' defined above will fire.
//gulp.task('default', ['test', 'watchTest', 'scripts', 'watch']);
gulp.task('default', ['test', 'watchTest']);

// Private Functions
// ----------------------------------------------------------------------------

var firstTestRun = true;
function bundleTest() {

    var _doIt = function () {
        // Browserify will bundle all our js files together in to one and will let
        // us use modules in the front end.    
        var testBundler = browserify({
            entries: './src/test/test.tsx',
            debug: true,

        }).plugin(tsify, {
            noImplicitAny: true,
            removeComments: true,
            preserveConstEnums: true,
            sourceMap: true,
            jsx: "react",
            target: "es2015"
        });
        // make the dependencies external so they dont get bundled by the 
        // app bundler. Dependencies are already bundled in vendor.js for
        // development environments.
        dependencies.concat(testDependencies).forEach(function (dep) {
            testBundler.external(dep);
        });

        testBundler
            // transform ES6 and JSX to ES5 with babelify
            .transform("babelify", { presets: ["es2015", "react"], extensions: ['.tsx', '.ts'] })
            .bundle()
            .on('error', gutil.log)
            .pipe(source('bundle-test.js'))
            .pipe(gulp.dest('./build/test/js/'))
            .pipe(mocha())
            .once('error', gutil.log);
    };

    if (firstTestRun) {
        firstTestRun = false;
        // Run Polyfill on mocha
        var polyfillMocha = browserify({
            entries: 'mocha_init.js'
        })
            .transform("babelify", { presets: ["es2015", "react"] })
            .bundle()
            .on('error', gutil.log)
            .pipe(source('bundle-test.js'))
            .pipe(gulp.dest('./build/test/js/'))
            .pipe(mocha())
            .once('error', gutil.log)
            .once('end', function () {
                _doIt();
            });
    } else {
        _doIt();
    }

}